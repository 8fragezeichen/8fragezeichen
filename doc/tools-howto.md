Bereits grob behandelt, s.a. #78 .

Nicht sonderlich schwer weil automatisiert, benötigt aber in der
derzeitigen Umsetzung als Shell-Script den Interpreter `sh` auf
einem POSIX-konformen System (z.B. GNU oder BSD). Angedacht war auch
ein Makefile, aber auch das Tool `make` haben die wenigsten Nutzer von
Nicht-POSIX-Systemen (z.B. Windows) bereits installiert und inzwischen
hat Microsoft Linux Tools (in Form eines Ubuntu) auf Windows bereit
gestellt.

1. [Einführung in Git](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)
2. [LaTeX-Kurs](https://wwwtcs.inf.tu-dresden.de/~borch/lehre/201516-latex/index.html), min. Einführung
3. Instaltion (unter Debian/Ubuntu z.B. mit `apt-get update && apt-get install ${pakete}`; wobei pakete eine Variable mit den Paketnamen ist und ${var} der Ausdruck um auf den Inhalt der Variable var zuzugreifen)
   * git (Pakete: `ssh git`)
   * pandoc, rsvg-convert und skalierbare Schriftarten (Pakete: `pandoc librsvg2-bin cm-super`)
   * texlive mit biber usw. (Pakete: `texlive texlive-latex-extra texlive-generic-extra texlive-lang-english texlive-lang-german biber texlive-bibtex-extra`)
4. In dein Verzeichnis für Projektdateien bzw. Dokumente auf deinem Rechner wechseln
5. `$ git clone https://gitlab.com/8fragezeichen/8fragezeichen.git CareCarden`
6. `$ cd CareGarden/doc/PH` – statt PH auch oder RFC, MAN, …
7. `$ ./convert-md2tex.sh`

Anschließend ist die PDF geöffnet und unter `CareGarden/doc/pub/` abgelegt.

Bitte gerne testen und Abweichungen melden. Oder wir gehen das gemeinsam durch.
