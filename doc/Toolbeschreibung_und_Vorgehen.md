**Toolbeschreibung und Vorgehen**
Anwendungen: ObjectiF
Diagramme: Anwendungsfalldiagramm, Aktivitätsdiagramm und Kontextdiagramm
Die Benutzung des Programmes ObjectiF war sehr intuitiv, 
da die Grundlagen zur Benutzung schon im 3. Semester in Software Engineering I gelegt u
nd in Software Engineering II nochmals gefestigt wurden. 
In ObjectiF haben die Verantwortlichen ein neues System mit dem Namen 
"CareGarden" und der Vorlage "UML with Java" angelegt. 
Des Weiteren wurden die Diagrammtypen Kontextdiagramm, 
Anwendungsfalldiagramm und Aktivitätsdiagramm angelegt. 
Die Hilfestellung des Programmes ObjectiF ist sehr gut gelöst, 
wodurch aufgetretene Probleme schnell gelöst werden konnten.