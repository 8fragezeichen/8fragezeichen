## Erstellung der FXML Datei

Als ersten Schritt erstellt man die View in Eclipse (vgl. \nameref{fig:PanlegenView}).
unter: 
**Rechtsklick $\rightarrow$ Package Views $\rightarrow$ neu
$\rightarrow$ others $\rightarrow$ new FXML Document.**
finden Sie die passende Option.

Vergeben Sie einen Namen und öffnen Sie diese mit Hilfe des
SceneBuilders.
**Rechtsklick $\rightarrow$ open with SceneBuilder**  

![View Patient anlegen \label{fig:PanlegenView}](raw/Bild_4_1.PNG "View Patient anlegen")


Vergeben Sie einen Name für den Controller der für diese View zuständig
sein soll. Dieser braucht noch nicht zu existieren, es muss jedoch bei
der späteren Erstellung der Controllerklasse darauf geachtet werden,
dass derselbe Name vergeben wird.
Ist bereits ein Controller angelegt, kann dieser ausgewählt werden. 

  
nun können Sie Ihre View nach belieben gestalten. Beachten Sie, dass bei
den bereits erstellten Views immer ein **AnchorPane** als Grundlage
verwendet wurde. Dies sollte beibehalten werden.
  
**Achtung!** Es muss der vollstaendige Packagepfad angegeben werden.
Bsp.: Controller.CPanlegen.
Für Elemente, auf die im Controller zugegriffen werden soll, muss eine
`fx:id` vergeben werden. Diese Option finden Sie im obigen Bild markiert.
