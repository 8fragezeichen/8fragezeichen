
# Dokumentation zum Testplan

Der Testplan ist Grundlage eines systematischen Vorgehens während des
Testens. Begriffe, die für uns einer Definition bedurften, sind im
Glossar näher beschrieben.

Der **Testplan** baut auf folgenden vier Säulen auf:

* Ressourcenplanung
* Teststrategie
* Testpriorisierung
* Werkzeugunterstützung

Hierbei wird bei diesem Projekt das Augenmerk auf **Teststrategie** und
**Testpriorisierung** gelegt, da insbesondre die **Teststrategie** die
Grundlage für den Testplan sowie für die durchzuführenden Tests bietet.
Diese lassen sich wie folgt unterteilen:

1. Komponententest
2. Integrationstest
3. Systemtest, Lasttest
4. Abnahmetest
	
Dabei ist die Reihenfolge der Auflistung einzuhalten. Der Lasttest ist
als integraler Bestanddteil des Systemtests zu betrachten, der
Abnahmetest ist als eigenständig anzusehen. Für *\projektname{}* wird
insbesondere der **Komponententest** und der **Integrationstest** von
Bedeutung sein, da Usability in diesem Fall eine höhere Wichtung
besitzt als beispielsweise Performanz und die beiden Testverfahren
geeignete Methoden bieten, um diese, sowie andere für *\projektname{}*
wichtige Faktoren, ausgiebig zu prüfen.

Basierend auf diesen Überlegungen ist folgender Aufbau (als erster
Grundriss) entstanden:
