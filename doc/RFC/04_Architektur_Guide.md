
# Architektur Guide

Hier soll, durch die Erstellung der Komponente Patient anlegen,
aufgezeigt werden, wie effizient mit der \nameref{fig:CGArchitektur}
gearbeitet werden kann.

Es wird dabei auf folgende Schritte eingegangen

 - FXML erstellen
 - Controller erstellen
 - Model erstellen  

![\projektname{} Architektur \label{fig:CGArchitektur}](raw/Bild_4_0.png "\projektname{} Architektur")  
\newpage
