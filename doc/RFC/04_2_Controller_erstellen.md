## Controller anlegen

Erstellen Sie den Controller für eine FXML Datei, in diesem Beispiel der
`VPanlegen`.
In der Architektur von *\projektname{}* ist es wichtig das dieser das
Interface **SuperView implementiert**.

**Hinweis ViewFactory**

`loadScreen()` lädt die FXML datei und  den Controller.
Dabei wird die instanz der Factory mittels der InterfaceMethode gesetzt.

setzen sie also die InterfaceMethode auf eine ViewFactoryinstanz in ihrem
Controller.

\nameref{fig:PFzuweisen}

![Factory zuweisen \label{fig:PFzuweisen}](raw/Bild_4_1_3.PNG "Factory zuweisen")

Zugriff auf Elemente der FXML datei mithilfe der `FX:ID`

\nameref{fig:PFXID}

![FXID \label{fig:PFXID}](raw/Bild_4_1_4.PNG "FXID")

`onAction`-Elemente werden ebenfalls so beschrieben, es wird lediglich eine
Methode angegeben. Der Methodenrumpf muss natürlich nach belieben
ausprogrammiert werden.

\nameref{fig:onAction}

![onAction \label{fig:onAction}](raw/Bild_4_1_5.PNG "onAction")
