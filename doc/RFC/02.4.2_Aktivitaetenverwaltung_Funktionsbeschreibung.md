
### Funktionsbeschreibung der Aktivitätenverwaltung

**Aktivität anlegen**

Um es dem Therapeuten zu ermöglichen, eine Aktivität anzulegen, muss er
den Button \enquote{Aktivitätenverwaltung} (Symbol Pflaster)
betätigen. Im neuen Fenster kann er bei Drücken des Buttons
\enquote{Aktivität anlegen} (Symbol Löwenzahn) eine neue Aktivität
erstellen.
Es müssen alle Felder befüllt sein, um den Button \enquote{anlegen} zum
Abschließen des Erstellungsvorganges zu aktivieren. Ist dies der Fall,
werden die Daten an die Datenbank gesendet. Bei erfolgreicher
Transaktion wird der Dialog \enquote{Aktivität erfolgreich angelegt}
angezeigt.

+---------------------------------------+---------------------------------+
| Funktion                              |  Bemerkung                      |
+=======================================+=================================+
| `public void submit()`                |  Erstellen der neuen Aktivität  |
+---------------------------------------+---------------------------------+

**Aktivität löschen**

Die Funktion \enquote{Aktivität löschen} kann in zwei Ansichten
durchgeführt werden. Es werden dabei zwei grundverschiedene
Zielstellungen umgesetzt. Einerseits soll das System es ermöglichen eine
Aktivität generell zu löschen, andererseits soll eine Aktivität
lediglich bei einem konkreten Patienten entfernt werden. Eine Aktivität
wird in der Ansicht \enquote{Aktivitätenverwaltung} (Drücken dieses Buttons)
gelöscht und damit generell aus der Datenbank entfernt werden. Für das
Löschen einer Aktivität wird der Button \enquote{Aktivität bearbeiten}
gedrückt. Nach Auswahl einer Aktivität und Betätigen des Buttons
\enquote{löschen} erfolgt der Bestätigungsdialog
\enquote{Aktivität wirklich löschen?}. Bei Drücken auf \enquote{OK} wird
die Aktivität gelöscht und somit aus Datenbank entfernt. Bei
erfolgreicher Transaktion erfolgt der Dialog
\enquote{Aktivität gelöscht!}. Sind in der Aktivitätenliste keine Daten
vorhanden, erfolgt ein Dialog \enquote{Keine Daten vorhanden!} und die
Aktion kann nicht ausgeführt werden.
Die Löschung einer Aktivität bei einem konkreten Patienten befindet sich
in der Ansicht \enquote{AktivitätenPlan} der
\enquote{Aktivitätenverwaltung}. Es muss ein Patient ausgewählt, der
List-Icon \enquote{Gesetzte Aktivitäten} ausgeklappt werden und die zu
löschende Aktivität ausgewählt werden. Durch Betätigen des Buttons
\enquote{Löschen} und Betätigen von \enquote{OK} in der eingeschobenen
Messagebox wird die Aktion ausgeführt.

+---------------------------------------+---------------------------------------------------------------------------------+
| Funktion                              |  Bemerkung                                                                      |
+=======================================+=================================================================================+
| `public void delete()`                |  Öffnet den Bestätigungsdialog und führt den Löschvorgang nach Bestätigung aus. |
+---------------------------------------+---------------------------------------------------------------------------------+

**Aktivitäten anzeigen**

Dient der Anzeige der Aktivitäten. Diese werden in der ListView angezeigt.

+-------------------------------------------+-------------------------------------------------------------------+
| Funktion                                  |  Bemerkung                                                        |
+===========================================+===================================================================+
| `public void loadActivity(String A_ID)`   |  Lädt die Aktivitäten, die dem Patienten im Plan zugeordnet sind. |
+-------------------------------------------+-------------------------------------------------------------------+
