
**Testplan**
: Die Testplanung erfolgt zu Beginn des Entwicklungsprojekts und wird, je nach Projektanpassung, während der gesamten Laufzeit stetig aktualisiert.

**Ressourcenplanung**
: Die Ressourcenplanung hat zum Ziel, den Aufwand an Mitarbeitern samt der von ihnen voraussichtlich benötigten Arbeitszeit, verwendeten Werkzeuge und anderer Hilfsmittel abzuschätzen und festzuhalten.

**Teststrategie**
: Die Teststrategie legt fest, welche Teile des Systems mit welcher Intensität getestet werden müssen. Eine Teststrategie ist notwendig, da ein vollständiger Test, d.h. ein Test, der alle Teile des
Systems mit allen möglichen Eingabewerten unter allen Vorbedingungen überprüft, in der Praxis nicht durchführbar ist.

**Abnahmetest**
: Der Abnahmetest dient zum Nachweis, dass die zwischen Auftraggeber und Auftragnehmer vereinbarten Leistungen erbracht wurden. Er bildet das zentrale Element der Abnahme.

**Testpriorisierung**
: Viele Projekte geraten gegen Ende unter einen erheblichen Zeitdruck, so dass nicht selten eingeplante Aktivitäten, und dazu gehört leider oft auch das Testen, nicht mehr durchgeführt werden
können. Da es nicht ratsam ist, ein Produkt auszuliefern, das auf kritische Fehlerwirkungen wegen Zeitmangels nicht getestet werden konnte, müssen solche kritischen Tests möglichst frühzeitig
durchgeführt werden. Eine Priorisierung der Tests ist somit notwendig, mit dem Ziel, die kritischen Systemteile zuerst zu testen.

**Werkzeugunterstützung**
: In diesem Teil der Testplanung wird der Tooleinsatz während des Testens geklärt. Es wird geprüft, inwieweit vorhandene Werkzeuge eingesetzt werden können und ob zusätzliche Tools angeschafft werden müssen.

**Komponententest**
: Der Komponententest prüft, ob jede einzelne Softwarekomponente für sich die Vorgaben seiner Spezifikation erfüllt. Charakteristisch für Komponenten ist, dass jede einzelne Komponente isoliert von anderen 
Komponenten des Systems überprüft wird. Dadurch soll verhindert werden, dass externe Einflüsse auf die einzelnen Komponente die Testergebnisse verzerren und jeder Fehler exakt der entsprechenden Komponente 
zugeordnet werden kann.

**Integrationstest**
: Ziel des Integrationstests ist es, die Kommunikation und das Zusammenwirken der zu größeren Teilsystemen zusammengefügten Komponenten zu überprüfen. Dabei werden Schnittstellenfehler aufgedeckt, die durch 
den Komponententest nicht entdeckt werden können.

**Usability-Test**
: Mit Hilfe von Usability-Tests wird die Nutzbarkeit überprüft. Hierbei wird u. a. geprüft, ob die an die Benutzungsoberfläche gestellten Designziele mit dem Entwurf erreicht werden. Es werden vor allem die 
Verlinkung, das Design und die Akzeptanz aus Sicht des Nutzers getestet.

**Funktionstests**
: Bei den Funktionstests werden unterschiedliche Anfangs- und Endbedingungen variiert und im Rahmen von Test-Cases getestet.

**Szenarientests**
: Siehe Funktionstests

**Systemtest**
: Innerhalb des Systemtests, bei dem das System als Ganzes in seiner Laufzeitumgebung getestet wird, wird die Erfüllung sowohl von funktionalen wie auch nichtfunktionalen Anforderungen geprüft.

**Lasttest**
: Der Lasttest ist integraler Bestandteil des Systemtests. Ziel eines Lasttests ist es, das System im erlaubten Grenzbereich auf Zuverlässigkeit zu testen und ob das Programm die zu erwartende
Systemlast bewältigen kann sowie die entsprechende Performance leistet.

**Black-Box-Verfahren**
: Beim Black-Box Verfahren wird das System als schwarzer Kasten betrachtet, dessen innere Struktur und Funktionalität dem Tester verborgen bleibt. Es ist deswegen kein Zugriff auf interne Operationen 
und Zustände von außen möglich, so dass sich der Tester auf die Betrachtung des Ein-/Ausgabeverhaltens beschränken muss. Testfälle können somit nur anhand extern sichtbarer Schnittstellen und der Spezifikation 
des Testobjekts abgeleitet werden.

**Glass-Box-Verfahren**
: Beim Glass-Box Verfahren stehen dem Tester im Gegensatz zum Black-Box Verfahren Informationen über den internen Aufbau des Testobjekts zur Verfügung. Die Testfallableitung beim Glass-
Box Verfahren orientiert sich an der Idee, möglichst viele Programmteile durch die Menge an Testfällen abzudecken, d.h. auszuführen.

**Testfall**
: Ein Testfall enthält die festgelegten Rahmenbedingungen, die zur Überprüfung eines Testlaufs benötigt werden. Zu den Rahmenbedingungen gehören beispielsweise:

* die für die Ausführung notwendigen Vorbedingungen,
* die Menge der Eingabewerte,
* die Menge der erwarteten Sollwerte,
* die Prüfanweisung (wie Eingaben an das Testobjekt übergeben und und Sollwerte abzulesen sind) und
* die erwarteten Nachbedingungen.
