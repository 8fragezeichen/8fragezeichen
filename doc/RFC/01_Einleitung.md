# Einleitung

Diese Entwicklerdokumentation wurde für Einarbeitung der Mitarbeiter
in die Software *\projektname{}* verfasst. Sie soll durch eine einfache
Strukturierung, helfen die Architektur der Klassen und deren
Zusammenwirken zu verstehen.
Die Gliederung ist nach Modulen bzw. Zuständigkeiten aufgebaut und stützt sich auf dieses \nameref{fig:packageDiagram}:

![Paketdiagramm \label{fig:packageDiagram}](raw/packageDiagram.png "Paketdiagramm")  
