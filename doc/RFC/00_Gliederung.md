\input{../titlepage}

\tableofcontents

\newpage

\pagenumbering{arabic}

\input{01_Einleitung} \newpage


\input{02_Komponenten} 

<!-- Zugangsverwaltung -->
\input{02.1_Zugangsverwaltung} 

\input{02.1.2_Funktionsbschreibung} \newpage

\input{02.1.3_Zugangsverwaltung_Klassendiagramm} \newpage

<!-- Patientenverwaltung -->

\input{02.2_Patientenverwaltung} 

\input{02.2.1_Patientenverwaltung_Uebersicht_Funktionalitaeten} 

\input{02.2.2_Patientenverwaltung_Funktionsbeschreibung} \newpage

\input{02.2.3_Patientenverwaltung_Klassendiagramme} \newpage


<!-- Therpeutenverwaltung -->

\input{02.3_Therapeutenverwaltung} 

\input{02.3.1_Therapeutenverwaltung_Uebersicht_Funktionalitaeten} 

\input{02.3.2_Therapeutenverwaltung_Funktionsbeschreibung} \newpage

\input{02.3.3_Therpeutenverwaltung_Klassendiagramme} \newpage

<!-- Aktivitätenverwaltung -->

\input{02.4_Aktivitaetenverwaltung} 

\input{02.4.1_Aktivitaetenverwaltung_Uebersicht_Funktionalitaeten} 

\input{02.4.2_Aktivitaetenverwaltung_Funktionsbeschreibung} \newpage

\input{02.4.3_Aktivitaetenverwaltung_Klassendiagramm} \newpage


<!-- ViewFactory -->

\input{02.5_ViewFactory} 

\input{02.5.1_ViewFactory_Uebersicht_Funktionalitaeten}

\input{02.5.2_ViewFactory_Funktionsbeschreibung} \newpage

\input{02.5.3_ViewFactory_Klassendiagramm} \newpage


<!-- Datenbank --> 

\input{02.6_Datenbank} 

\input{02.6.1_Datenbank_Uebersicht_Funktionalitaeten} \newpage

\input{02.6.2_Dankenbank_Funktionsbeschreibung} \newpage

\input{02.6.3_Datenbank_Klassendiagramm} \newpage


<!-- Aktivitartenplan --> 

\input{02.7_Aktivitaetenplan} 

\input{02.7.1_Aktivitaetenplan_Uebersicht_Funktionalitaeten} 

\input{02.7.2_Aktivitaetenplan_Funktionsbeschreibung} \newpage

\input{02.7.3_Aktivitaetsplan_Klassendiagramm} \newpage


\addtocontents{toc}{\protect\newpage}

<!-- Tesplan --> 

\input{03_Testplan} \newpage

\input{03.1.1_Komponententest} \newpage

\input{03.1.2_Integrationstest} \newpage

\input{03.1.3_Systemtest} \newpage

\input{03.1.4_Abnahmetest} \newpage

\input{03.1_Teststrategie} \newpage

\input{03.2_Testdurchlauf} \newpage

<!-- Architektur Guide --> 

\input{04_Architektur_Guide} \newpage

\input{04_1_FXML_Datei} \newpage

\input{04_2_Controller_erstellen} \newpage

\input{04_3_Model_erstellen} \newpage

\section*{Glossar}
\addcontentsline{toc}{section}{Glossar}

\pagenumbering{Alph}

\input{Glossar} 
