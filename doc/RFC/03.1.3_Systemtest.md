
## Systemtest

Folgende Verfahren können alle Bestandteil des **Systemtests** sein:

 * Funktionstest
 * Sicherheitstest
 * Interoperabilitätstest
 * Leistungstest
 * Performanztest
 * Speicherverbrauchtest (oder Ähnliches)
 * Recovery Testing
 * Usability-Test
 * Stresstest
 
Viele dieser Tests sprengen den Umfang des Projektes,
**Usability-Tests** und **Funktionstests** werden im
**Integratoinstest** weitestgehend abgedeckt. Der **Sicherheitstest**
wäre in Hinblick auf den Datenschutz unbedingt ausführlich
durchzuführen, würde aber aufgrund des notwendigen Umfangs den Rahmen
des Projektes sprengen.
Dennoch sollten in diesem Fall zumindest rudimentäre Funktionalitäten,
wie Passwortabfragen und Eingaben, sowie Sichtbarkeit innerhalb der
einzelnen Rollen unbedingt geprüft werden.

Gerade im Punkt **Systemtest** sind die Entwickler der einzelnen
Programmteile angehalten, mindestens die Hauptfunktionen ihres Codes zu
testen (Eingaben, überschriebene Variablen, undefiniertes Verhalten).
