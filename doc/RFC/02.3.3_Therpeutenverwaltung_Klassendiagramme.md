
### Klassendiagramme der Therapeutenverwaltung

\nameref{fig:KDPwaendern}

![Klassendiagramm Passwort aendern \label{fig:KDPwaendern}](raw/Bild_2_3_3_1.png "Klassendiagramm Passwort aendern")

\newpage

\nameref{fig:KDDatenanzeigen}

![Klassendiagramm Daten anzeigen \label{fig:KDDatenanzeigen}](raw/Bild_2_3_3_2.png "Klassendiagramm Daten anzeigen")  

\newpage
