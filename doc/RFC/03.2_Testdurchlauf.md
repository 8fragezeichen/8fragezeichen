
## Testdurchlauf

### Testdurchlauf \enquote{\projektname{}} 2016-05-31

Allgemeine Anmerkungen:

Glossar:

<Name>-Button$\rightarrow$ bedeutet, dass der Button mit der entsprechenden Bezeichnung geklickt wurde. (Damit nicht ständig "foo-Button klicken.." im Protokoll steht)

Globale Probleme:

    (1) Beenden Button für das Programm
    (1) Button-Beschrifung: Konsistenz in der Sprache, entweder Deutsch ODER Englisch

**Anmeldung**

Eingabe:

    Username: fooo     PW: fooo

Eingabe:

   Username: @#\^\^\^\^#W#     PW: @#$\^\^\^\^

Eingabe:

   Username: @#\^\^\^\^#W#     PW: @#$\^\^\^\^

Eingabe:

   Username: ccc.....cc (ca. 500 Zeichen)     PW: @#$\^\^\^\^

Eingabe:

   Username: Tcwolf    PW: ccc.....cc (ca. 500 Zeichen)

**Verhalten** : Button grün, Fehlermeldung, Zurückleitung zur Anmeldeseite, Inputdaten vorhanden.

Eingabe:

    Username: Tcwolf   PW: cwolf

**Verhalten**: Weiterleitung zur Anmeldeseite

**Einschätzung**:

    (4) Fehlermeldung: Ggf. Unterscheidung, ob Nutzername oder Passwort falsch waren
    (4) Zurückleitung zur Anmeldeseite: Passwortfeld , ggf. auch Nutzername, aus der
        Eingabe entfernen

**Home**

Eingabe: Home-Button

**Verhalten**: Anzeigen der Therapeuteninformationen

Eingabe: Home-Button$\rightarrow$Passwort ändern

    Altes PW: cwolf   Neues PW: foo

Eingabe: Home-Button$\rightarrow$Passwort ändern

    Altes PW: foo     Neues PW: foo

**Verhalten**: Button grün, Weiterleitung zum Änderungsfeld, Äderung durchführbar, Änderung erfolgreich, Prüfung über Neuanmeldung

Einschätzung:

    (2) Mindestlänge für Passwort fordern
    (3) Eingabefelder zwecks besseree Lesbarkeit etwas weiter auseinander.
    (4) Verhindern dass Altes PW == Neues PW

**Patientenverwaltung**

**Patient anlegen**

Eingabe:

    Vorname: Anna    Nachname: Bahr
    Nick: abahr
    Str: bahrstr     Hausnr: 21
    PLZ: 88888       Ort: Dresden
    Tel: 12345
    Diagnose: foobar
    Krankenkasse: huk

**Verhalten**: Fehler beim Erstellen, Zurückleitung zum Eingabefeld, alle Datenfelder wieder leer

Einschätzung:

    (1) Bei Eingabe von normalen Daten Fehlermeldung
    (1) Keine konkrete Fehlermeldung (Name falsch? PLZ falsch?)
    (1) Eingabefelder leer nach Zurückleitung, ärgerlich aus Usabilitysicht

**Patient anzeigen**

**Patient bearbeiten**

Eingabe:

Patient mit Maus auswählen $\rightarrow$ Bearbeiten-Button

    Strasse: Nummer geändert
    Diagnose: Diagnose geändert

$\rightarrow$ Ändern-Button

**Verhalten**: Abfrage, ob Daten geädert werden sollen, bei ok-Button$\rightarrow$ erfolgreiche Änderung, bei cancel$\rightarrow$ Button folgt Abbruch

Einschätzung:

    (2) Diagnose-Feld in der unternen rechten Ecke, Position unintuitiv, besser zu
        den anderen Feldern packen

**Patient löschen**

Eingabe: Patient mit Maus auswählen $\rightarrow$ Löschen-Button

**Verhalten**: (Bei voller Liste) Abfrage, ob Patient gelöscht werden soll, bei ok-Button$\rightarrow$ erfolgreich, bei cancel$\rightarrow$ Button folgt Abbruch

(Bei leerer Liste) Meldung, dass keine Daten vorhanden seien

Einschätzung:

    (3) Optional: Mehrere Patienten zum Löschen auswählen können
    (4) Bei leerer Liste auch dem Bearbeiten-Button eine entsprechende Meldung
        beifügen

### Testdurchlauf \enquote{\projektname{}} 2016-05-31

Allgemeine Anmerkungen:

Glossar:

<Name>-Button$\rightarrow$ bedeutet, dass der Button mit der entsprechenden Bezeichnung geklickt wurde.

Globale Probleme:

    (1) Beenden Button für das Programm
    (1) Button-Beschrifung: Konsistenz in der Sprache, entweder Deutsch ODER Englisch

**Anmeldung**

Eingabe:

    Username: fooo    PW: fooo

Eingabe:

    Username: @#\^\^\^\^#W#     PW: @#$\^\^\^\^

Eingabe:

    Username: @#\^\^\^\^#W#     PW: @#$\^\^\^\^

Eingabe:

    Username: ccc.....cc (ca. 500 Zeichen)     PW: @#$\^\^\^\^

Eingabe:

    Username: Tcwolf            PW: ccc.....cc (ca. 500 Zeichen)

**Verhalten**: Button grün, Fehlermeldung, Zurückleitung zur Anmeldeseite, Inputdaten vorhanden.

Eingabe:

    Username: Tcwolf            PW: cwolf

**Verhalten**: Weiterleitung zur Anmeldeseite

Einschätzung:

    (4) Fehlermeldung: Ggf. Unterscheidung, ob Nutzername oder Passwort falsch waren
    (4) Zurückleitung zur Anmeldeseite: Passwortfeld , ggf. auch Nutzername, aus der
        Eingabe entfernen

**Home**

Eingabe: Home-Button

**Verhalten**: Anzeigen der Therapeuteninformationen

Eingabe: Home-Button$\rightarrow$Passwort ändern

    Altes PW: cwolf             Neues PW: foo

Eingabe: Home-Button$\rightarrow$Passwort ändern

    Altes PW: foo               Neues PW: foo

Verhalten: Button grün, Weiterleitung zum Änderungsfeld, Äderung durchführbar, Änderung erfolgreich, Prüfung über Neuanmeldung

Einschätzung:

    (2) Mindestlänge für Passwort fordern
    (3) Eingabefelder zwecks besseree Lesbarkeit etwas weiter auseinander.
    (4) Verhindern dass Altes PW == Neues PW

**Patientenverwaltung**

**Patient anlegen**

Eingabe:

    Vorname: Anna      Nachname: Bahr
    Nick: abahr
    Str: bahrstr       Hausnr: 21
    PLZ: 88888         Ort: Dresden
    Tel: 12345         Diagnose: foobar
    Krankenkasse: huk

**Verhalten**: Fehler beim Erstellen, Zurückleitung zum Eingabefeld, alle Datenfelder wieder leer

Einschätzung:

    (1) Bei Eingabe von normalen Daten Fehlermeldung
    (1) Keine konkrete Fehlermeldung (Name falsch? PLZ falsch?)
    (1) Eingabefelder leer nach Zurückleitung, ärgerlich aus Usabilitysicht

**Patient anzeigen**
**Patient bearbeiten**

Eingabe:

- Patient mit Maus auswählen $\rightarrow$ Bearbeiten-Button
- Strasse: Nummer geändert
- Diagnose: Diagnose geändert
- $\rightarrow$ Ändern-Button

**Verhalten**: Abfrage, ob Daten geädert werden sollen, bei ok-Button$\rightarrow$ erfolgreiche Änderung, bei cancel$\rightarrow$ Button folgt Abbruch

Einschätzung:

    (2) Diagnose-Feld in der unternen rechten Ecke, Position unintuitiv, besser
        zu den anderen Feldern packen

**Patient löschen**

Eingabe: Patient mit Maus auswählen $\rightarrow$ Löschen-Button

**Verhalten**: (Bei voller Liste) Abfrage, ob Patient gelöscht werden soll, bei ok-Button$\rightarrow$ erfolgreich, bei cancel$\rightarrow$ Button folgt Abbruch

(Bei leerer Liste) Meldung, dass keine Daten vorhanden seien

Einschätzung:

    (3) Optional: Mehrere Patienten zum Löschen auswählen können
    (4) Bei leerer Liste auch dem Bearbeiten-Button eine entsprechende Meldung beifügen

### Testdurchlauf \enquote{\projektname{}} 2016-06-02

Allgemeine Anmerkungen:

Glossar:

<Name>-Button$\rightarrow$ bedeutet, dass der Button mit der entsprechenden Bezeichnung geklickt wurde. (Damit nicht ständig "foo-Button klicken.." im Protokoll steht)

Globale Probleme:

    (1) Button-Beschrifung: Konsistenz in der Sprache, entweder Deutsch ODER Englisch
    (1) Konsistene Button-Plazierung
    (2) Konsistene Animation bei ähnlichen Funktionen

**Aktivitätenverwaltung**

**Aktivität anlegen**

Eingabe: Vorhandener Patient:

- Name: Meier
- Kategorie: Rückenübung
- Kurzbeschreibung: Reha Woche 1
- Beschreibung: pizza

**Verhalten** : Aktivität erfolgreich angelegt

Eingabe: Nicht vorhandener Patient:

- Name: Miezekatze
- Kategorie: Rückenübung
- Kurzbeschreibung: Reha Woche 1
- Beschreibung: pizza

**Verhalten** : Aktivität erfolgreich angelegt

Einschätzung:

    (1) Wenn ein Patient nicht in der Datenbank steht, sollte er nicht angelegt
        werden können
    (2) Bei Eingabefeld "Name": Ist damit Vorname oder Nachname oder Beides
        gemeint?
    (3) Aus Usability-Sicht wäre es angenehm, wenn man vom Anlegen direkt zur
        Anzeige gehen könnte ohne den Zwischenschritt des Zurückgehens

**Aktivität anzeigen**

**Aktivität bearbeiten**

Eingabe:

- Aktivität mit Maus auswählen $\rightarrow$ Bearbeiten-Button
- Kurzbeschreibung: Name geändert
- Beschreibung: Beschriebung geändert
- Ändern-Button

**Verhalten** : Abfrage, ob Daten geändert werden sollen, bei ok-Button$\rightarrow$ erfolgreiche Änderung, bei cancel$\rightarrow$ Button folgt Abbruch, die Daten werden auch gelöscht, aber in der Eingabemaske ist noch die unerwünschte Änderung zu sehen.

Einschätzung:

    (1) Wenn Daten geändert werden sollen und man dies abbricht, unbedingt bei
        der Zurückführung darauf achten, dass die nicht erwünschten Änderungen
        auch nicht mehr in der Eingabemaske sichtbar sind. (Auch wenn das
        Löschen in der DB erfolgreich war)
    (2) Aus Konsistenzgründen: Button-Animationen für Patientenfunktionen und
        Aktivitätenfunktionen angleichen.
    (2) Aus Konsistenzgründen: Button-Plazierung üebrall angleichen, dh. entweder
        immer unten rechts, mittig oder links
    (3) Aus Usability-Sicht ist es verwirrend, wenn man aus dem Bearbeitungsmenü
        auf den Löschen-Button klicken kann, dieser erstmal wieder zur überliegenden
        Ebene führt und man ihn dort erneut bestätigen muss, um zum Löschmenü zu
        kommen. Eine direkte Weiterleitung wäre in dem Fall intuitiver.
    (4) Beschreibung-Feld und Kurzbeschreibung-Feld ggf mit etwas mehr Abstand
        zwischen Eingabe und Schrift

**Aktivität löschen**

Eingabe: Aktivität mit Maus auswählen $\rightarrow$ Löschen-Button

**Verhalten** : (Bei voller Liste) Abfrage, ob Patient gelöscht werden soll, bei ok-Button$\rightarrow$ erfolgreich, bei Button Cancel folgt Abbruch

(Bei leerer Liste) Meldung, dass keine Daten vorhanden seien

Einschätzung:

    (2) Optional: Mehrere Äktivitäten zum Löschen auswählen können

**Aktivitätenplan**

Eingabe: Patient Drop-Down auswählen $\rightarrow$ PMeier

    Aktivität: A0001     Start: 5/31/2016     Ende: 6/15/2016

Verhalten: Bei Betätigen des Add-Buttons wird Aktivität angelegt

Eingabe: Patient Drop-Down auswählen $\rightarrow$ PMeier

    Aktivität: A0001     Start: 6/2/2016      Ende: 5/2/2016

**Verhalten** : Bei Betätigen des Add-Buttons wird Aktivität angelegt

Einschätzung:

    (1) Im Patienten-Drop-Down sind Patienten vorhanden, die bereits nicht mehr
        in der Datenbank vorhanden sind
    (1) Wählt man nun einen nicht-vorhandenen Patienten, ist das möglich (darf
        nicht möglich sein), der Aktivitätenplan wird aber für einen anderen,
        in der Datenbank noch vorhandenen Patienten angelegt, das führt zu
        fehlerhaften Daten
    (1) Abbruchdialog für Add-Button beifügen
    (1) Button zum Aktivitätsplan anzeigen wäre sehr gut, damit man nicht
        jedesmal alles ausfüllen muss, wenn man z.B nur etwas bearbeiten oder
        löschen will
    (2) Fehlermeldung "Bitte füllen Sie alle Felder aus" wird beim Anlegen
        bereits angezeigt, * wirkt unfreundlich, da man ja gar nicht alles
        ausgefüllt haben kann, wenn man erstmalig einen Aktivitätsplan anlegt.
        Besser wäre es, wenn sie nur dann eingeblendet wird, wenn der Anlegende
        auf den Add-Button klickt und etwas vergessen haben sollte
    (2) Es sollte nicht möglich sein, das Startdatum später als das Enddatum
        einzustellen
