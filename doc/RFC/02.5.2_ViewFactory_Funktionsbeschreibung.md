
### Funktionsbeschreibung der ViewFactory

Im Speziellen handelt es sich hierbei um eine abgewandelte Form der
Fabrikmethode als Grundlage.
Die ViewFactory-Klasse hält eine Reihe von instanzierten Views mit deren
zugehörigen Controllern in einer HashMap.
Alle Controller implementieren wiederum das Interface
\enquote{SuperView}, welches das Überschreiben der setParent-Methode
vorgibt.
Zur Laufzeit ist es nun möglich, die Views über die Fabrik zu laden und
auszutauschen.

+----------------------------------------+-------------------------------------------------------------------------------+
| Funktion                               |  Bemerkung                                                                    |
+========================================+===============================================================================+
| `private void addScreen(String name,   |  Hinzufügen der Node zu bestehender HashMap                                   |
| Node mode, SuperView con)`             |                                                                               |
+----------------------------------------+-------------------------------------------------------------------------------+
| `public void loadScreen(String name,   |  JavaFX-Routine zum Laden des FXML-Dokumentes und des zugehörigen Controllers |
| String node)`                          |                                                                               |
+----------------------------------------+-------------------------------------------------------------------------------+
| `public void reloadScreen(String name, |  Aktualisierung der Node in der HashMap                                       |
| String node)`                          |                                                                               |
+----------------------------------------+-------------------------------------------------------------------------------+
| `public Node getScreen(String name)`   |  Rückgabe der geladenen Node                                                  |
+----------------------------------------+-------------------------------------------------------------------------------+
