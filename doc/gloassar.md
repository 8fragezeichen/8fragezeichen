*Markdown*
    Eine Markup-Sprache, viel simpler und lesbarer als HTML

*SVG*
    Scaleable Vector Graphics ist ein Dateiformat für Vektorgrafiken

*PDF* 
    Portable Dokument Format ist ein Dateiformat für Plattformübergreifende und i.A. Papiertreue Darstellung

*Asana*
    Ein Webservice zur Unterstützung der Projektorganisation (u.a. mit Gantt-Diagramm)

*Gannt Chart*
    Eine Grafische Darstellung von voneinander abhängigen Arbeitspaketen (s.a. Issue) zur Zielerreichung

*Issue*
    Ein Arbeitspaket, kann in weitere zerfallen; häufig in Form einer Problembeschreibung mit Möglichkeit der Diskussion einer Lösung

*ERM*
    Entity Relationship Model: Das Datenmodell für die Datenbank abgeleitet von den Datenstrukturen aus der Analysephase
