Hier soll erfasst werden, welche kommunikationsmittel vorhanden sind um ggf. abhängig von Funktionen zu wechseln.

* Vorhanden: '(+)'
* Benutzt: '+'
* Verweigert: '-'
* Bevorzugt: '+++'

Messenger   | Eric | Felix | Florian | Kilian | Richard | Uta | Wolf
:----------:|:----:|:-----:|:-------:|:------:|:-------:|:---:|:----:
git         |      |       |         |        |         |     | +
Asana       |      |       |         |        |         |     | (+)
EMail       | +    | +     |         | +      | +       | +   | ++
Whatsapp    | +    | +     | +       | +      | (+)     | +   | (+)
Telegram    |      |       |         |        |         |     | (+)
Jabber/XMPP |      |       |         |        |         |     | +
Threema     |      |       |         |        |         |     | +
