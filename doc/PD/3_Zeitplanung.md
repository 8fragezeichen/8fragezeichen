# Zeitplanung

## Planerstellung

Einen ersten groben Plan konnte ich bereits vor Themenvergabe erstellen,
da die Abgabe des Systems in die Prüfungszeit des Semesters fällt und
somit der Zeitraum für die Bearbeitung schon einmal eingegrenzt werden
konnte.
Nach endgültiger Themenvergabe, wurde der Projektplan aufgestellt.
Hierbei war es wichtig mit ein wenig Luft nach hinten zu planen, um
Stress möglichst gering zu halten.

## Vorgehensmodell

Als Vorgehensmodell entschieden wir uns als Gruppe für ein iteratives
Vorgehen nach dem Spiralmodell.
Da es einzelne Komponenten zu entwickeln galt, erschien uns dies am
sinnvollsten.

*Hinweis*:
: Diese Art der komponentenbasierten Entwicklung, erfordert ein hohes
Maß an Disziplin von allen Beteiligten. Des Weiteren möchte ich an
dieser Stelle darauf hinweisen, dass aufgrund der Wahl eines eigenen
Projektes, die Anforderungen einem ständigen Revisionsprozess
unterliegen. Analyse, Entwurf, Implementierung sind hier besonders
gefordert. Besonders wenn es darum geht alte Entwürfe zu verwerfen und
neue Wege zu gehen.

Mein Dank gilt hier dem Durchhaltevermögen aller Beteiligten.

\newpage

## Werkzeuge

Grundlegende Philosophie bei der Zeitplanung ist, die direkte Zuweisung
von Aufgaben an **genau ein** Mitglied.
Dies ist sinnvoll, da so gewährleistet ist, dass sich immer genau ein
Mitglied für anstehende Aufgaben verantwortlich fühlt.


### Asana

Die Zeitplanung erfolgte initial mittels \enquote{Asana}.
Asana selbst ist ein Projektmanagementwerkzeug, welches
Aufgabenverteilung an bestimmte Gruppenmitglieder ermöglicht.

![Asana \label{fig:Asana}](raw/Asana.png "Asana Bedienoberfläche")


### GitLab

Im Projektverlauf wurden die digitalisierten Dokumente zunächst in
geteiltem Online-Speicher eingepflegt und die Arbeitsdokumente dann nach
und nach in Git überführt.
Die Versionsverwaltung ist für die meisten Projektbeteiligten ein
ungewohntes Hilfsmittel dessen Nutzen sich nicht unmittelbar erschlossen
hat.
Durch die Verwendung von GitLab als grafische Oberfläche konnte jedoch
auch für die Aufgabenplanung und Problemerfassung bis hin zu Lösungen
der ursprüngliche Grobplan aus dem im Asana bereitgestellten
Gantt-Diagramm bald in einzelnen Meilensteinen auf GitLab überführt
werden um dort auch flexibel zu reorganisieren und leichter bedienbar
einzelne Problemstellungen zu bearbeiten.
Gerade bei Änderungen zum Pflichtenheft und konnte hier die
Nachvollziehbarkeit sicher gestellt werden. Ohne eine Versionierung wäre
der Überblick über die Änderungen sonst viel zu schnell verloren
gegangen.


## Meilensteinplan

Um Termine und Zwischenziele klar darzustellen wurden Meilensteine
aufgestellt.
Anschließend wurden die einzelnen Aufgaben priorisiert und in Issues
Personen und den Meilensteinen zugeordnet.

| Ablaufdatum | Meilenstein           |
|-------------|-----------------------|
| 2016-03-21  | Kick-Off              |
| 2016-04-01  | Entwurf Dokumentation |
| 2016-04-11  | Analyse               |
| 2016-04-18  | Datenbank             |
| 2016-04-29  | Pflichtenheft         |
| 2016-04-17  | Prototyp              |
| 2016-05-02  | Protokolle            |
| 2016-06-10  | Vorträge              |
| 2016-06-17  | Implementation        |
| 2016-06-17  | Benutzerhandbuch      |
| 2016-06-17  | Dokumentation         |
| 2016-06-29  | Übergabe              |
