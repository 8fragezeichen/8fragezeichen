# Projektziele

## Problembeschreibung

### SOLL   

> \enquote{\textbf{Kommunikation verbessern, Brücken bauen}, dem Körper und dem Geiste etwas Gutes tun. Die sich rasant weiterentwickelnde, vernetzte Welt hält auch im medizinischen Bereich Einzug. Doch während sich die IT-Welt immer rascher wandelt, wird noch nach Lösungen gesucht, um den direkten Patientenkontakt nicht unterbrechen zu lassen. Eine Weiterführung von Therapiemaßnahmen ist oft zu kostspielig und die Kommunikation bleibt in den meisten Fällen auf der Strecke.}
Auszug Pflichtenheft (vgl. \cite{misc:PHCareGarden})

### IST  

Das Endprodukt ermöglicht die Erstellung von Aktivitätsplänen und
Aktivitäten. Dem Patienten werden seine zu erledigenden Aufgaben
angezeigt.
Damit erfüllt es den Kommunikationsgedanken des Ausgangsproblems. Des
Weiteren ist das System funktional erweiterbar um auf Änderungen
reagieren zu können.

## Humanziele

### SOLL

<!-- hier die Verwendung einer Definitionsliste, das kommt aus einem extended Markdown -->

**Arbeitsplatz**
: Der Arbeitsplatz sollte für jeden frei wählbar sein. Jeder sollte in seinem optimalen kreativen Umfeld arbeiten dürfen.

**Arbeitszeit**
: Die messbare Arbeitszeit sollte sich auf die Gruppenbesprechungen bzw. die spontanten Zusammenkünfte.

**Entgeltgestaltung**
: Alle Beteiligten einigten sich auf einen Vergütungsverzicht aufgrund der Liebe zum Projekt.

**Arbeitsorganisation**
: Die Arbeitsorganisation sollte möglichst einfach und zentral erfolgen.
Erläuterung etwaiger Tools erfolgt später.

### IST

**Arbeitsplatz**
: Der Arbeitsplatz wurde meist frei gewählt. Ledigliche spontane Gruppenzusammenkünfte wurden in Räumlichkeiten der HTW/SLUB   

**Arbeitszeit**
: Die Arbeitszeit wurde individuell den eigenen Bedürfnissen angepasst.
  
**Entgeltgestaltung**
: Alle Beteiligten einigten sich auf einen Vergütungsverzicht aufgrund der Liebe zum Projekt.

**Arbeitsorganisation**
: Die Organisation erfolgte in mehreren Tools. Bei Unklarheiten, bestand die Möglichkeit der Kontaktaufnahme über Mail/Telefon.

### Projektschnittstellen

Die beteiligten Personen haben jeder einen eigenen Verantwortungsbereich.
In diesem soll die Problemlösung selbstständig erfolgen können.
Die Arbeitsleistung für einen Verantwortungsbereich wird hingegen
gemeinsam erbracht.
Die Aufgaben werden vom Verantwortlichen an andere verteilt und bei
Konflikten entscheidet der Projektleiter an wen die Aufgabe aufgrund
Auslastung oder Kompetenz erteilt wird.

Die erkannten Schnittstellen zwischen den Verantwortungsbereichen führen
zu einer erhöhten Kommunikation zwischen denselben. Hier jeweils nach
Priorität die wichtigsten drei aufgeführt.

**Implementation**, **Datenbank**
: Analyse, Test, Dokumentation

**Qualitätsmanagement**
: Dokumentation, Test, Implementation

**Analyse**
: Entwurf, Implementation, Datenbank

**Entwurf**
: Analyse, Implementation, Datenbank

**Test**
: Qualitätsmanagement, Implementation, Dokumentation

**Dokumentation**
: Analyse, Qualitätsmanagement, Implementation

**Projektleitung**
: Alle

<!-- tbc -->
