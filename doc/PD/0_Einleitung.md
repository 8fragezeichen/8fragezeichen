# Einleitung

Hinweis für den Leser: Da im Folgenden wiederholt das Wort Projektdokumentation zu lesen sein wird, erlaube ich mir die Abkürzung PD für die weitere Behandlung des Themas zu verwenden.

## Projektdokumentation - Was ist das?

Dieses Dokument dient in erster Linie der Reflexion des Projektverlaufs. Sie soll zukünftige Projekte vereinfachen und Probleme, die sich evtl. im Kontext dieses Projektes ergeben haben, in späteren Projekten vermeiden.

Das Deutsche Institut für Normung (vgl. \cite{DIN:Projektmanagement}) beschreibt die PD als:

> \enquote{Zusammenstellung ausgewählter wesentlicher Daten über Konfiguration, Organisation, Mitteleinsatz, Lösungswege, Ablauf und erreichte Ziele des Projektes.}

## Wie ist eine PD strukturiert?

Der Aufbau dieses Dokumentes richtet sich im Groben an den chronologischen Ablauf des bearbeiteten Projektes.

Der Abschnitt **Projektvorfeld** befasst sich mit dem IST-Zustand und der Aufgabenstellung des Projektes.

Die **Projektziele** stellen einen SOLL-IST-Vergleich dar.

Im Abschnitt **Zeitplanung** geht es im Wesentlichen darum, wie die Projektarbeit organisiert war und umfasst wesentliche Bestandteile des Projektmanagements.
