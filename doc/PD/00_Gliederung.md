\input{../titlepage}

\tableofcontents

\newpage

\pagenumbering{arabic}

\input{0_Einleitung} \newpage

\input{1_Projektvorfeld} \newpage

\input{2_Projektziele} \newpage

\input{3_Zeitplanung} \newpage

\input{4_Wirtschaftlichkeit} \newpage

\input{5_Gruppendynamik_und_Fazit}

\newpage

\printbibliography[
	title={Literaturverzeichnis}
]

\addcontentsline{toc}{section}{Literaturverzeichnis}
