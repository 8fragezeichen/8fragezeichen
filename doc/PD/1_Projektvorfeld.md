# Projektvorfeld  

> \enquote{Was gewesen ist, brauche ich nicht zu vergessen und nicht zu
> verleugnen. Ich will es im Herzen bewahren als Erfahrung und als Hilfe
> für die Zukunft.} (Aphorismus)

## Ist-Analyse

Während dieses Kapitels befasse ich mich größtenteils mit dem Zustand,
der vor Beginn des Projektes vorlag.

## Gemeinsam sind wir stark

Ein Projekt wäre kein Projekt ohne ein paar Gruppenmitglieder. Doch die
Auswahl kann auf die verschiedensten Arten erfolgen. Eine grundsätzliche
Herangehensweise gibt es dabei nicht und die Fachpresse versucht diesem
Problem auf unterschiedlichste Weise zu begegnen.

Da Entscheidungen in meinem Fall einen gewissen Balanceakt zwischen
**Herz**, **Kopf** und **Bauch** darstellen, entschied ich mich für eine
erste Ausschreibung des Gruppenbedarfs in sozialen Netzwerken, um rein
formell jedem Kommilitonen die gleichen Möglichkeiten zu bieten, an
unserem Projekt teilhaben zu können.
Da der Kopf somit abgehakt war, folgten einige Entscheidungen Bauch und
Herz betreffend.   
Bereits im WS 15/16 hatte ich gute Erfahrungen mit den Mitgliedern der
Gruppe im Modul SE I gemacht und wusste, dass eine harmonische
Zusammenarbeit mit ihnen möglich ist.
Desweiteren war hier die fachliche Kompetenz ebenfalls gegeben und ein
konstruktiver Diskurs sollte so ermöglicht werden.
Hinzu kam, dass Kommilitonen aus dem vorigen Jahrgang bereits ein
Scheitern erlebt hatten und wir die Chance aus den Fehlern von anderen
zu lernen wahrnehmen und unsere Ränge komplettieren wollten.

## Aufgabenstellung

Zur Auswahl standen in diesem Semester diverse Projekte
(vgl. \cite{misc:Belegarbeitsthemen}), von der Erstellung eines
Bibliothekssystems, bis hin zu einer Liftsteuerung, war ein großes
Spektrum gegeben.

Alternativ konnte eine eigene Aufgabenstellung bei Professor Hauptmann
vorgeschlagen werden.

### Eigenes Projekt - Wozu?

> Wenn Wissenschaft kein Wissen schafft
(\cite{Zeitjung2016:Wissenschaft})

Als Gruppe entschieden wir uns ein eigenes Projekt zu bearbeiten.
Der Hintergrund der Entscheidung für eines eigenen Projektes erfolgte
aufgrund von drei managementtechnischen Entscheidungen:

1. Wir wollten kein Projekt, das bereits in Vorjahren bearbeitet wurde, da evtl. im Netz kursierende Lösungen den kreativen Eigenanteil senken könnten.
2. Unser Ziel war auch eine möglichst realistische Simulation des kreativen Schaffensprozesses anhand einer neuartigen Idee.
3. Eine ständige Reflexion des aktuellen Standes und Revision der bisherigen Umsetzung im Vergleich zu den Anforderungen.

Es gab anfangs mehrere Vorschläge und grobe Ideen.
Ein Vorschlag von Neda mit Bezug zur Interaktion zwischen Therapeuten
und Patienten führte zu einer ersten Idee.
Diese wurde in der Gruppe in verschiedene Richtungen weitergedacht und
anschließend auf gemeinsame Ziele reduziert.
Hierbei ging die Einigkeit in der Gruppe über die Zielerreichung als
Motivation der Beherrschbarkeit und Komplexität vor.

Der Vorschlag wurde in der Gruppe positiv aufgenommen und somit war der
Projektstart erfolgreich gelungen.

### Konkrete Aufgabenstellung

In Zusammenarbeit mit Prof. Hauptmann, in der Rolle des Aufraggebers,
wurde die Aufgabenstellung für die bisherige Projektauswahl an unsere
Idee angepasst.
Dazu war es notwendig sowohl unsere Idee als auch die Vorgaben teilweise
zu konkretisieren um ein gemeinsames Verständnis für das eigentliche
Projektziel zu entwickeln.
Das Ergebnis (vgl. \cite{misc:Aufgabenstellung}) wurde am 11.04.2016
schriftlich festgeschrieben und für unsere Zwecke digitalisiert.
