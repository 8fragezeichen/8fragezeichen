# Gruppendynamik und Fazit

Zentrales Ziel der Gruppenzusammenarbeit war die Bereitstellung einer Arbeitsumgebung, die kreatives Schaffen ermöglicht.
Dies wurde während des gesamten Bearbeitungszeitraums durch aktive Beiträge aller Gruppenmitglieder unterstützt und so bestand meine Aufgabe lediglich in der zeitlichen Organisation der vorgeschlagenen Teamaktivitäten.

## Teamaktivitäten

**Gruppenfrühstück**
Ein durch unsere Qualitätssicherung vorgeschlagenes Frühstück fand nach Abschluss der Projektarbeit in den Räumen der HTW statt.

**Spontane Meetings**
Auch außerhalb vereinbarter Gruppentreffen, fanden in spontan vereinbarten Treffen Diskussionsrunden statt. Hier wurde gemeinsam an Themen gearbeitet, die besonders knifflig zu lösen waren oder die noch Verbesserungsbedarf hatten.

**Gruppenfoto**
Um unsere gemeinsame Arbeit für die Nachwelt festzuhalten, entschieden wir uns für ein Gruppenfoto.

![gruppenfoto \label{fig:gruppenfoto}](raw/gruppenfoto.JPG "gruppenfoto")

## Fazit
Ausgehend von dem Ziel ein SW-Projekt erfolgreich durchzuführen und dies mit möglichst entspannt und planvoll zu erreichen, kann ich eine positive Bilanz ziehen.
Die Gruppenzusammenstellung ermöglichte eine produktive und Zielführende Arbeit, wobei die individuellen Fähigkeiten jedes einzelnen einen wesentlichen Beitrag zum Endprodukt beigetragen haben.
Abschließend kann ich nur sagen, dass eine Projektarbeit mit diesen Mitgliedern jederzeit wieder möglich ist und bedanke mich hiermit nocheinmal im Rahmen der Projektdokumentation bei unserem Analysten Richard alias Thorsten, unsererm Gitzauberer Wolf, unserem fleißigen Implementierer Felix alias Axel, unserem Datenbankbeauftragten Kilian alias Lutz, unserer kritischen Qualitätsbeauftragten Uta alias \enquote{Uta die Große}, Neda unserer unermüdlichen Testerin und abschließend unserem Künstler und Entwerfer Florian.  

\vspace{1.5cm}

---------------------------------------------------
Ihr Projektleiter \newline
Eric Brandt alias Rüdiger von der Hohenlohe


  

