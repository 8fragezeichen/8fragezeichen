
# Wirtschaftlichkeit

Ungeachtet der freien Lizensierung stellt die Wirtschaftlichkeit einen
nicht unwesentlichen Teil der Projektdokumentation dar.
Es fand in unserem Projekt eine individuelle Zeitplanung ohne Erfassung
der Arbeitszeiten statt. Um dennoch eine kleine Kostenplanung zu
simulieren werde ich im Folgenden auf den Modulplan zurückgreifen.

## Kostenplanung

Laut Studienordnung setzt sich die Studienzeit im Modul I-151 SE II aus
4 SWS Präsenzzeit und 90 Stunden Selbststudienzeit zusammen.
Da ich die Präsenzzeit nicht in Rechnung stellen kann, werde ich mit den
90h pro Verantwortungsbereich kalkulieren.

**Ausgangsdaten**

- 8 Mitglieder
- 90 Stunden Selbststudienzeit
- $30\text{\,EUR}$ Stundensatz pro Mitglied

Daraus ergeben sich folgende **Personalkosten**:

$8 * 90\text{\,h} * 30 \text{\,EUR} = 21.600 \text{\,EUR}$

Hinzu kommen **Schulung** für etwaige Systeme (Git, Eclipse, Asana)
Hierfür ergeben sich in den Verantwortlichkeiten der Dokumentation(Git),
Implementierung(Eclipse) und Projektleitung(Asana) noch folgende Kosten
die als Weiterbildungsaufwand zu Buche schlagen:

* Git Third-Level-Support: $2.500\text{\,EUR}$ für die gesamte Projektlaufzeit
* Eclipse First-Level-Support: $300\text{\,EUR}$
* Asana First-Level-Support  : $300\text{\,EUR}$

Es ergeben sich Selbstkosten in Höhe von:

$21.600\text{\,EUR} + 2.500\text{\,EUR} + 300\text{\,EUR} + 300\text{\,EUR} = 24.700\text{\,EUR}$

\newpage

## Amortisationsrechnung

Grundlage bildet hier eine einfache Handelskalkulation ausgehend von Selbstkosten in Höhe von $24.700,00\text{\,EUR}$

Posten           | Betrag
:----------------|--------------------:
Selbstkosten     | $24.700,00\text{\,EUR}$
+Gewinn 25\%     |  $6.175,00\text{\,EUR}$
Barverkaufspreis | $30.875,00\text{\,EUR}$
Provision 4\%    |  $1.286,46\text{\,EUR}$
Listenpreis      | $32.161,46\text{\,EUR}$
USt 19\%         |  $6.110,68\text{\,EUR}$
Brutto           | $38.272,14\text{\,EUR}$

Zu klären gilt es hierbei, wann das Geld fließen soll. Decken wir die
Selbstkosten bis zur Abgabe des Projekts und steigen somit mit maximalem
Risiko ein oder vereinbart man eine Teilzahlung nach
Fortschrittsetappen?
Dies gilt es beim nächsten Projekt abzuklären.

## Fazit

Es ist immer schwierig in einem Studienprojekt eine genaue
Wirtschaftlichkeit zu berechnen. Die Simulation mit einigermaßen
reellen Werten zeigt jedoch, dass hinter einem SW-Prozess vorallem
Know-How steckt, welches bezahlt werden will.

Um noch eine für uns relevantere Formel aufzustellen, könnte man die
Wirtschaftlichkeit auch recht einfach wie folgt berechnen:

$\text{Wk}(\projektname{}) = \frac{\text{Input}}{\text{Output}} = \frac{\text{Leistung aller Mitglieder} + \text{Vorerfahrung}}{\text{Gruppennote} + \text{Erfahrungsgewinn}}$
