**Aufgabenbereiche der Beteiligten**

<!--

 Aufgabenbereich    | Rufzeichen | Bibl.-Nr.
:-------------------|:----------:|----------:
 Projektleitung     | Rüdiger    | s72784
 Analyse            | Torsten    | s72817
 Entwurf            | Florian    | s68410
 Implementation     | Axel       | s72827
 Dokumentation      | Wolf       | s72785
 Qualitätssicherung | Uta        | s70989
 Datenbank          | Kilian     | s72854
 Test               | Neda       | s68378

-->

Aufgabenbereich     | Bibl.-Nr.
:-------------------|----------:
 Projektleitung     | s72784
 Analyse            | s72817
 Entwurf            | s68410
 Implementation     | s72827
 Dokumentation      | s72785
 Qualitätssicherung | s70989
 Datenbank          | s72854
 Test               | s68378