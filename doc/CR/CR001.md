# CR001

## 1. Submitter – General Information

* Number of CR:	{CR 001 – Issue 65}
* Type of CR:	Klarstellugn des Bezuges zu einer Grafik
* Project/Program/Initiative:	CareGarden / Pflichtenheft
* Submitter: 	Uta (QM)
* Brief Description: Der Bezug zur "Folgenden" Abbildung geht verloren, weil der Seitenumbruch dazwischen funkt
* Date Submitted:	2016-05-30
* Date Required:	2016-06-06
* Priority: Medium
* Reason for Change:	Bessere Verständlichkeit
* Affected Artifacts: Kapitel 4.2
* Date: 2016-05-30

## 2. Initial Analysis (Responsibility Manager)

* Time impact: 5min
* Schedule impact: none
* Cost impact: none
* Comments: Seitenumbruch ist nicht das Problem, sondern die Formulierung; Vorschlag zur Behebung liegt an.
* Manager: Wolf (doc)
* Date:	2016-05-30

## 3. Change Decision (Project Manager)

* Decision:	Approved
* Comments:	Klarstellung übernehmen
* Manager: Rüdiger (ProjLtr)
* Date:	2016-05-31

## 4. Concrete Change

Unter Kapitel 4.2

* Streiche: "(\\autoref{fig:RuppSceme}) sind nach folgendem Schema" , setze:  "sind nach dem Schema der \\autoref{fig:RuppSceme}"

## 5. Change Applied

* By: Uta (QM)
* Date: 2016-06-03
