# CR006

Da Patient sich nicht selbst Aktivitäten zuweisen können sollte (Fehlendes Wissen zur medizinischen Notwendigkeit).

Alternativen:

1. streichen, ab Nr. 21 neu nummerieren
2. Umformulierung auf **sollte**

Die Funktionalität wird erst mit einem Individualplan benötigt.
Dieser wurde bereits sehr früh nicht mehr für die aktuelle Version vorgesehen.
Daher macht die Implementation erst Sinn, wenn dieser in einer kommenden Verion eingeführt werden sollte.

## 1. Submitter – General Information

* Number of CR:	{CR006 – Issue 087}
* Type of CR:	{Enhancement / Defect}
* Project/Program/Initiative:	CareGarden / Pflichtenheft
* Submitter: 	Felix Brandt
* Brief Description:	Da Patient sich nicht selbst Aktivitäten zuweisen können sollte (Fehlendes Wissen zur medizinischen Notwendigkeit).
* Date Submitted:	06.06.2016
* Date Required:	06.06.2016
* Priority:	High
* Reason for Change: fehlerhaftes PH
* Affected Artifacts:	Abschnitt 7 Pflichtenheft
* Comments:  ---
* Date:	06.06.2016

## 2. Initial Analysis (Responsibility Manager)

* Time impact: 5min
* Schedule impact: ---
* Cost impact: ---
* Comments: ---
* Date:	06.06.2016

## 3. Change Decision (Project Manager)

* Decision:	Genehmigt
* Comments:	in Absprache mit Implementierer
* Date:	06.06.2016

## 4. Concrete Change

In PH Kapitel 4.2.2, Nr. 22 der Satzschablonen
* Ändere von "soll" auf "sollte"
Nr. 17 auf wird, da suche bei Patienten erst mit Individualplan Sinn macht.

## 5. Applied

* By: Wolf
* Date: 2016-06-07
