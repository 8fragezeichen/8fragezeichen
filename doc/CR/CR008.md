# CR008

## 1. Submitter – General Information

* Number of CR:	{CR 008 – Issue 89}
* Type of CR:	Erläuterung der Ausgabe 4.1.4
* Project/Program/Initiative:	CareGarden / Pflichtenheft
* Submitter: 	Uta (QM)
* Brief Description: Es bestanden Unklarheiten bzgl. des Ausgabeverhaltens
* Date Submitted:	2016-06-06
* Date Required:	2016-06-06
* Priority: Medium
* Reason for Change:	Bessere Verständlichkeit
* Affected Artifacts: Kapitel 4.1.4
* Date: 2016-05-30

## 2. Initial Analysis (Responsibility Manager)

* Time impact: 5min
* Schedule impact: none
* Cost impact: none
* Comments: --
* Manager: Wolf (doc)
* Date:	2016-05-30

## 3. Change Decision (Project Manager)

* Decision:	Approved
* Comments:	Klarstellung übernehmen
* Manager: Rüdiger (ProjLtr)
* Date:	2016-06-07

## 4. Concrete Change

Unter Kapitel 4.2

Ergänze: "Sofern die Eingaben bei der Verarbeitung zu Problemen führen, z.B. nicht dem notwendigen Wertebereich entsprechen, werden Fehler mit einer für den Benutzer hilfreichen Beschreibung ausgegeben."

## 5. Change Applied

* By: Eric (PrtLtr)
* Date: 2016-06-08
