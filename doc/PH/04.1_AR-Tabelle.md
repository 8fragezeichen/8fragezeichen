Die Auslöse-Reaktion-Tabelle verwaltet alle voneinander unabhängigen Funktionen des Softwaresystems \projektname{}.
Neben dem **Namen der Funktion** zeigen **Ein-** und **Ausgangsdaten** den Datenfluss der Funktion auf.
**Bemerkungen** weisen auf Besonderheiten bei der Nutzung der Funktion hin.

* P: Patient
* T: Therapeut
* Eingabe: Eingangsdaten
* Ausgabe: Ausgangsdaten

### Patientendaten verwalten

Die Verwaltung der Patientendaten\g{}  dient der Erstellung, Bearbeitung und Änderung von Daten.
Die Administrationshoheit liegt zum Großteil auf der Therapeutenseite.

* PDat: Patientendaten
* PDet: Patienten-Details

+----------------------+------------------+---------------------+------------------------+
| Name der Funktion    |  Eingabe         |  Ausgabe            | Bemerkung              |
+======================+==================+=====================+========================+
| PDat anlegen         | Anmeldewunsch    | Zugangsdaten        | Von T                  |
+----------------------+------------------+---------------------+------------------------+
| PDat ändern          | Patientendaten   |                     |                        |
+----------------------+------------------+---------------------+------------------------+
| PDat löschen         | Löschwunsch      | Bestätigung drucken | Von T                  |
+----------------------+------------------+---------------------+------------------------+
| PDat anzeigen(T/P)   | PDat             | PDet                |                        |
+----------------------+------------------+---------------------+------------------------+

### Therapeutendaten verwalten

Die Therapeutenverwaltung dient der **Anzeige**, **Änderung** und
**Erstellung** von Daten, die den Therapeuten betreffen.

Hierbei ist zu beachten, dass die Funktionen "T anlegen" und "T löschen"
durch den Betreiber, während der Einrichtung des Systems realisiert
werden.
Dies dient vor allem der Sicherheit und Kontrolle über die Nutzer des
Systems.

* T: Therapeut
* TDat: Therapeutendaten
* TDet: Therapeuten-Details
* TInf: Therapeuteninformation
* Best: Bestätigung

+----------------------+------------------+----------------+------------------------+
| Name der Funktion    |  Eingabe         |  Ausgabe       | Bemerkung              |
+======================+==================+================+========================+
| T anlegen            | Anmeldewunsch    | Best           | d. Betreiber eingepfl. |
+----------------------+------------------+----------------+------------------------+
| TInf anzeigen(P/T)   | TDat             | TDet           |                        |
+----------------------+------------------+----------------+------------------------+
| TInf anzeigen(P/T)   | TDat             |                |                        |
+----------------------+------------------+----------------+------------------------+
| TDat löschen         | Löschwunsch      | Best drucken   | d. Betreiber           |
+----------------------+------------------+----------------+------------------------+
| Passwort ändern      | Änderungswunsch  | Best           |                        |
+----------------------+------------------+----------------+------------------------+

### Aktivitätsdaten verwalten

Die Aktivitätsdatenverwaltung dient der **Erstellung**, **Bearbeitung** und **Anzeige**
von Aktivitäten\g{} . Die Erstellung von Aktivitäten obliegt dem behandelnden Therapeuten.


* Akt: Aktivität
* AktList: Aktivitätenliste
* AktDat: Aktivitätsdaten
* Best: Bestätigung

+----------------------+-------------------+---------+--------------------------+
|Name der Funktion     |   Eingabe         |  Ausgabe| Bemerkung                |
+======================+===================+=========+==========================+
|Akt erstellen(T)      | AktDat            | Best    | Erstellt T               |
+----------------------+-------------------+---------+--------------------------+
|AktList anzeigen(P/T) |                   | AktList |                          |
+----------------------+-------------------+---------+--------------------------+
|Akt suchen(P)         | Suchanfrage       |         |                          |
+----------------------+-------------------+---------+--------------------------+
|Akt bearbeiten(T)     | Bearbeitungswunsch| Best    |                          |
+----------------------+-------------------+---------+--------------------------+
|Akt abschließen(P)    | Abschlusswunsch   | Best    | P bestätigt              |
|                      |                   |         | Abschluss der Aufgabe    |
+----------------------+-------------------+---------+--------------------------+
|Akt hinzufügen(T/P)   | Akivitätsvorschlag|         | T gibt Pflichtplan vor,  |
|                      |                   |         | P ergänzt Individualplan |
+----------------------+-------------------+---------+--------------------------+
|Akt löschen           | Löschwunsch       | Fehler  |                          |
+----------------------+-------------------+---------+--------------------------+

### Aktivitätsplan verwalten

Die Verwaltung von Aktivitätsplänen umfasst die **Erstellung**, **Bearbeitung** und 
**Anzeige** von Plänen.

Sofern die Eingaben bei der Verarbeitung zu Problemen führen, z.B. nicht dem notwendigen Wertebereich entsprechen, werden Fehler mit einer für den Benutzer hilfreichen Beschreibung ausgegeben.

* AktPlan: Aktivitätsplan
* IndPlan: Individualplan
* AktDat: Aktivitätsdaten

+----------------------+------------------+----------------+-------------+
| Name der Funktion    |   Eingabe        |  Ausgabe       | Bemerkung   |
+======================+==================+================+=============+
| AktPlan anlegen(T)   | AktDat           | Fehler         |             |
+----------------------+------------------+----------------+-------------+
| AktPlan bearbeiten(T)| AktDat           | Fehler         |             |
+----------------------+------------------+----------------+-------------+
| AktPlan löschen(T)   | AktDat           | Fehler         |             |
+----------------------+------------------+----------------+-------------+
| IndPlan anlegen      | AktDat           | Fehler         | Optional    |
+----------------------+------------------+----------------+-------------+
| IndPlan bearbeiten   | AktDat           | Fehler         | Optional    |
+----------------------+------------------+----------------+-------------+
| IndPlan löschen      |                  | Fehler         | Optional    |
+----------------------+------------------+----------------+-------------+

### Nachrichtendaten verwalten

Die Verwaltung der Nachrichtendaten dient der **Anzeige** und dem **Senden** von Nachrichten
zwischen Patient und Therapeut. Sie dient der Realisierung der Kommunikationsanforderung.

+----------------------+------------------+----------------+-------------+
| Name der Funktion    |   Eingabe        |  Ausgabe       | Bemerkung   |
+======================+==================+================+=============+
| Nachricht senden     | Sendewunsch      |                |             |
+----------------------+------------------+----------------+-------------+
| Nachricht anzeigen   | Anzeigewunsch    |                | d. System <!--(z.B. 2min, oder aktiv)--> |
+----------------------+------------------+----------------+-------------+

### Datenauswertung verwalten

Die Datenauswertung dient der **Auswertung** der abgeschlossenen Aktivitäten über der Zeit.

* GB: Gesamtbilanz
* KL: Kennzahlenliste

+-------------------+------------------+----------------+-------------+
| Name der Funktion |   Eingabe        |  Ausgabe       | Bemerkung   |
+===================+==================+================+=============+
| GB anzeigen       | Anfrage          | GB             |             |
+-------------------+------------------+----------------+-------------+
| KL anzeigen       | Anfrage          | KL             |             |
+-------------------+------------------+----------------+-------------+

### Zugang verwalten

Die Zugangsverwaltung dient der Regelung des Systemzugangs.

* PDat: Patientendaten
* TDat: Therapeutendaten

+------------------------+------------------+---------------------------+-------------+
| Name der Funktion      |   Eingabe        |  Ausgabe                  | Bemerkung   |
+========================+==================+===========================+=============+
| Anmelden (Login)(P/T)  | TDat, PDat       | Bestätigung/Fehlermeldung |             |
+------------------------+------------------+---------------------------+-------------+
| Abmelden (Logout)(P/T) |                  | Bestätigung/Fehlermeldung |             |
+------------------------+------------------+---------------------------+-------------+
