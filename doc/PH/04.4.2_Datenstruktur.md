
Termin = {
	Startzeit,
	+ Endzeit
}

Aktivität = {
	A_ID,
	+ Gruppe,
	+ Titel,
	+ Beschreibung,
	+ (Preis),
	+ Bewertung(Kennzahl),
	+ Termin,
	+ Aktivitätsdauer,
	+ Zuordnung
	  (betreut/selbständig)
}

Nachricht = {
	Therapeut,
	+ Aktivität (Betreff)
	+ Patient,
	+ Sendezeit,
	+ Empfangszeit,
	+ Inhalt
}
