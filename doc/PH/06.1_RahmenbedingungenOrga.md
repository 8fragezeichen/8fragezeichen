Organisatorische Rahmenbedingungen beschreiben die Art und Weise, wie die Prozesse in organisatorische Strukturen eingebunden sind.
Für dieses Projekt sind folgende organisatorischen Rahmenbedingungen festgelegt: 

* Der Therapeut muss für eine Anmeldung am System einen schriftlichen Antrag an die Firma "8fragezeichen" stellen. Über diese ist die Anmeldeadministration organisiert. Der Anmeldewunsch enthält die im Datenstrukturdiagramm zum Therapeuten festgehaltenen Daten (vgl. \autoref{Subsec:Datenstruktur}).

* Ein Patient wird vom Therapeuten angelegt. Der Therapeut vergibt die Zugangsdaten
(Passwort und Nutzerkennung) an den Patienten.


