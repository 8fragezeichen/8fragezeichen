# Funktionale Anforderungen \label{funktionaleAnforderungen}

Das im folgenden beschriebene System enthält verschiedene funktionale Anforderungen(\cite{Pohl:Basiswissen}).
Dies sind die lt. Anforderungsanalyse erarbeiteten Anforderungen bzw. Eigenschaften, welches das System dem Anwender bieten muss. Zur Beschreibung von Anforderungen bedient sich dieses Pflichtenheft folgender Hilfsmittel der objektorientierten Modellierung:

* Auslöser-Reaktionen-Tabelle
* Darstellung mittels Satzschablone nach Chris Rupp
* Use-Case Diagramme
* Datenstruktur(\cite[S.\,215\,ff]{Sommerville:SE})
* Aktivitätsdiagramme

## Auslöser-Reaktionen-Tabelle

\input{04.1_AR-Tabelle}

## Satzschablonen nach Chris Rupp

\input{04.2_Satzschablonen}

## Use-Case Diagramme

Anwendungsfalldiagramme stellen, als Bestandteil der UML (Unified Modeling Language), ein weiteres Mittel der Systembeschreibung dar.
Ziel dieser Art der Modellierung ist die Darstellung der Beziehungen zwischen Akteuren und Funktionalitäten des Softwaresystems.
Zur Vereinfachung wurden die Diagramme hierarchisch aufgebaut und bilden das System auf folgende Weise ab:    

\input{04.3_AWF}

\clearpage

## Datenstruktur \label{Subsec:Datenstruktur}

Die Datenstruktur beschreibt den Aufbau der im System fließenden Daten.

<!-- -->

\renewcommand{\lstlistingname}{}

\begin{multicols}{2}

\lstinputlisting[numbers=none, caption={}, title={}, label={}, language=]{04.4.1_Datenstruktur.md}

\vfill
\columnbreak

\lstinputlisting[numbers=none, caption={}, title={}, label={}, language=]{04.4.2_Datenstruktur.md}

\end{multicols}


<!-- -->

<!--
\begin{multicols}{2}


\end{multicols}
-->

<!--
\input{04.4_Datenstruktur}
-->


\clearpage

## Aktivitätsdigramme


\input{04.5_AktDia}

