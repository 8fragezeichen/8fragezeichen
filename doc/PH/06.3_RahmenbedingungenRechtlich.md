Die rechtlichen Rahmenbedingungen können Einfluss auf Anforderungen
für das Projekt haben. Deshalb betrachten wir, welche Regelungen 
für uns relevant sind und gestalten vor diesem Hintergrund.

<!-- tl;dr:

* Datenübertragung spez. B-PersDat, [BDSG §3 IX] \enquote{Gesundheitsdaten}
  + Auf dem Gerät
  + insb. i.d. zentralen Datenbank
* Einwilligung der Nutzers (Opt-In)
* Allgemeine Informationspflichten/Anbieterkennzeichnung AKA \enquote{Impressumspflicht}, [TMG §5]
  + f. 8Fragezeichen
  + je Terapeuth 
* Lizenzen
  + Entwicklungsumgebung/Ausführungsumgebung: Custom/Java? 
  + Herausgabe Code/Dokumentation: [AGPL] / [CC-BY-SA]

[BDSG §3 IX]: https://dejure.org/gesetze/BDSG/3.html
[TMG §5]: https://dejure.org/gesetze/TMG/5.htm
[AGPL]: https://www.gnu.org/licenses/agpl-3.0
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/3.0/de/

-->


### Integrität und Vertraulichkeit

Für Gesundheitsdaten besteht nach [BDSG §3 IX] \enquote{Gesundheitsdaten} ein erhöhter Schutzbedarf (BPersDat).

<!--

Vorschlag f. Maßnahme:

Insbesondere die eventuelle Speicherung auf dem Nutzergerät und die Übertragung von Daten vom Nutzergerät an die zentrale Datenbank des Betreibers muss hierzu betrachtet werden.
-->

### Zweckbestimmung

Nutzer könnten Einwände gegen die Erhebung und Verarbeitung von anderen personenbezogenen Daten haben. Auch die Übertragung an einen Dienstleister sowie die Beauftragung eines Dienstleisters (Outsourcing) kann Mitteilungspflichtig sein.

<!--

Vorschlag f. Maßnahme:

Die Einwilligung der Nutzer in die Datenverarbeitung (Opt-In) muss sichergestellt werden. Hierbei muss dem Nutzer der Zweck der Erhebung, Speicherung und Verarbeitung bekannt gemacht werden und die erfolgte Einwilligung entsprechend dokumentiert werden. Davon darf nicht ohne erneute Einwilligung abgewichen werden.
-->

### Anbieterkennzeichnung

Nach [TMG §5] besteht eine Allgemeine Informationspflichten-/Anbieterkennzeichnung \enquote{Impressumspflicht}. Dies gilt sowohl für die Betreiber als auch für beworbene Nutzer (z.B. Therapeuten).

<!--

Vorschlag f. Maßnahme:

1. Eigenes Impressum, auch als ``humans.text``
2. Impressum für Therapeuten
   a) Einwilligung bei Registrierung einholen
   b) aus Stammdaten generiertes Impressum anzeigen
-->

### Lizenzen

> \enquote{Nutzer sollen *die Freiheit* haben Software auszuführen,
> zu kopieren, zu verbreiten, zu untersuchen, zu ändern und zu
> verbessern} (Richard Martin Stallman).

Wir halten innerhalb unseres Projektes fest :

1. welche Bedingungen wir bei der Nutzung von Werkzeugen einhalten müssen und
2. unter welchen Bedingungen unsere Arbeitsergebnisse von anderen genutzt werden können 

\newpage

#### Festlegungen Dritter  

Die Vorgaben Dritter sind für uns verbindlich. Werkzeuge, die wir nutzen
sind auch entsprechend zu lizensieren. Hilfreich ist im Hochschul-Umfeld,
dass zumeist eine Nutzung für die Lehre kostenfrei zur Verfügung gestellt wird.

<!--

ToDo:

1. verwendete Tools eintragen und
2. Lizenz ermitteln

-->

Produkt/Werkzeug | Lizenz
-----------------|-------------------
[objectiF]       | [Proprietär, Produkt-Spezifisch](http://www.microtool.de/en/objectif-rm-tool-for-requirements-engineering/pricing/), (HTW-Labore)
[asana]          | [Proprietär, Produkt-Spezifische ToS](https://asana.com/terms#terms-of-service) (Webservice)
[InstaGantt]     | [Proprietär, Produkt-Spezifische ToS](https://www.instagantt.com/terms.html) (Webservice)
[GitLab-CE]      | [GitLab-CE License] (MIT-ähnlich, Webservice)
[Git]            | [GPL V2] (Freie Software, Copyleft)
[Pandoc]         | [GPL V2] (Freie Software, Copyleft)
[TeX]            | [TeX License] (Freie Software)
[LaTeX]          | [LPPL] (Freie Software)
[MySQL]          | [MySQL Licensing](https://www.mysql.com/about/legal/)
[Java 8]         | [Oracle Binary Code License Agreement](http://www.oracle.com/technetwork/java/javase/terms/license/)


#### Bestimmungen unseres Projektes

Trotz zumeist kostenfreier Lizenzen für Studierende ermöglicht
[Freie Software] zusätzlich die vier Freiheiten. Unter ethischen
Aspekten wird diese daher von uns bevorzugt. Auch rechtlich betrachtet
ergeben sich hierduch für uns keine unangenehmen Verpflichtungen oder
Konsequenzen.

Aus den gleichen Gründen entgegnen wir der Fehlentwicklung des
Urheberrechtes (und Copyright) mit Verwendug der CC-Lizenzen von
[Creative Commons].


<!--
f. Dokumentation festgelegt
Vorschläge vom 18.04.
ergänzt um Logo am 27.04.
-->

Bestandteil   | Lizenz
--------------|------------------------
Dokumentation | [CC-BY-SA]
Logo          | [CC-BY-ND]
Code          | [AGPL V3]

<!-- ### Verweise -->

[BDSG §3 IX]: https://dejure.org/gesetze/BDSG/3.html
[TMG §5]: https://dejure.org/gesetze/TMG/5.htm
[Freie Software]: https://fsfe.org/about/basics/freesoftware.de.html
[Creative Commons]: https://creativecommons.org/
[AGPL V3]: https://www.gnu.org/licenses/agpl-3.0
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/
[CC-BY-ND]: https://creativecommons.org/licenses/by-nd/4.0/
[GPL V2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0
[objectiF]: http://www.microtool.de/objectif/en/index.asp
[asana]: https://app.asana.com/
[InstaGantt]: http://instagantt.com/
[GitLab-CE]: https://gitlab.com/gitlab-org/gitlab-ce/
[GitLab-CE License]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/LICENSE
[Git]: https://git-scm.com/
[Pandoc]: http://pandoc.org/
[TeX]: http://www-cs-faculty.stanford.edu/%7Eknuth/
[TeX License]: https://en.wikipedia.org/wiki/TeX_license#License
[LaTeX]: http://www.latex-project.org/
[LPPL]: https://www.latex-project.org/lppl/
[MySQL]: https://www.mysql.com/
[Java 8]: https://www.oracle.com/java/index.html