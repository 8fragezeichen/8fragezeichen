# Fehlertoleranz \label{SecFehlertoleranz}

> \enquote{Wer einen Fehler gemacht hat und ihn nicht korrigiert, begeht einen zweiten.} (Konfuzius)

Fehlertoleranz beschreibt nach Definition die Fähigkeit eines Systems,
nach Auftreten eines Fehlverhaltens, seine Funktionsweise beizubehalten
und auf eintretende Fehler entsprechend zu reagieren.

## Kategorisierung

Das folgende Schema dient zur Orientierung für die Kategorisierung der auftretenden Fehler.

+--------------------+--------------------------------------------------------------+----------------------------------------------------------+
| Fehlerkategorie    | Auslöser                                                     | Reaktion                                                 |
+====================+==============================================================+==========================================================+
| Systemfehler       | Verbindungsabbruch zur Datenbank,                            | Entsprechende Fehlerausgabe,                               |
+--------------------+--------------------------------------------------------------+----------------------------------------------------------+
|                    | Absturz des Systems,                                         | (REDO/UNDO) Rollback,                                    |
+--------------------+--------------------------------------------------------------+----------------------------------------------------------+
|                    | Höhere Gewalt                                                | Recoverymaßnahmen                                        |
+--------------------+--------------------------------------------------------------+----------------------------------------------------------+
| Bedienfehler       | Keine Aktion führt zur Abschaltung der kritischen Funktionen	| Fehlerausgabe am Bildschirm mit entsprechenden Hinweisen |
+--------------------+--------------------------------------------------------------+----------------------------------------------------------+

Dabei werden Systemfehler mit 5 gewichtet und Bedienfehler mit 2.

## Fehlerbehandlung

Die Fehlerbehandlung verläuft in folgenden Teilschritten:

1. Entdeckung des Fehlers
2. Lokalisierung des Fehlers
3. Anzeige der Fehlermeldung

Zu 1. : Mögliche Fehler sind in dem Entwurfsprozess einzuarbeiten und im gesamten Modellierungsprozess zu beachten.

Zu 2. : Mögliche Fehlerquellen sollten durch entsprechendes Exceptionhandling abgefangen werden. Dabei ist darauf zu achten, dass die Fehlerausgabe eine Rückverfolgung des Problems ermöglicht.

Zu 3. : Fehler der Kategorie 1 und 2 werden dem Benutzer durch Ausgabe am Bildschirm deutlich gemacht.

\newpage

+-------------------+------------------------------------------------------------------------------------------------+
| Begriff           | Bedeutung                                                                                      |
+===================+================================================================================================+
| REDO              | Wiederholung aller abgeschlossenen und nichtabgeschlossenen Transaktionen ab einem Zeitpunkt t |
+-------------------+------------------------------------------------------------------------------------------------+
| UNDO              | Rückgängigmachen von Änderungen, die bis zu einem Zeitpunkt t erfolgt sind                     |
+-------------------+------------------------------------------------------------------------------------------------+
| ROLLBACK          | Nutzt REDO/UNDO, um im Zeitpunkt an dem Daten korrekt sind, wiederherzustellen                 |
+-------------------+------------------------------------------------------------------------------------------------+
| CATCH             | Ausnahmebehandlung, falls Fehler auftreten                                                     |
+-------------------+------------------------------------------------------------------------------------------------+

## Kennzahlen

Während des Testens wird auf folgende Kennzahlen besonderes Augenmerk gelegt.(\cite{Schaefer:QM})

+------------------+----------------------------------------------------------+
|Kennzahl          |                Beschreibung                              |
+==================+==========================================================+
|Laufzeiteffizienz |     Zeit bis Programm gestartet ist                      |
+------------------+----------------------------------------------------------+
|Fehlerbeschreibung|     Klarheit der Fehlerausgabe                           |
+------------------+----------------------------------------------------------+
|Laufzeitverhalten | Auszuführende Funktion wird wie vorhergesehen ausgeführt |
+------------------+----------------------------------------------------------+

## Tolerierbare Fehler

Unter Berücksichtigung der festgelegten Fehlerkategorien werden folgende Toleranzen vereinbart.
Summe der Fehler sollte unter 30 Punkten liegen.
Davon sollte höchstens ein Systemfehler auftreten.


