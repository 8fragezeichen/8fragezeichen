# Schnittstellen \label{Sec:Schnittstellen}

## User Interface

Dem Benutzer wird das System mehrere Schnittstellen\g{}  zur Eingabe und Änderung von Daten zur Verfügung stellen. Nach einem Login-Vorgang wird der Nutzer, je nach Rolle\g{} , ein Menü vorfinden, um mit dem System zu interagieren.
Der erste Entwurf sah eine Schnittstelle für den Login nach dem Grobentwurf des \nameref{fig:Login}\g{}  vor:  

![GUI-Login \label{fig:Login}](raw/GUI_Login.jpg "GUI Login")  
  
Die Schnittstelle für die Kernfunktionalitäten wird in Anlehnung an den Entwurf des \nameref{fig:MainMenu}s aufgebaut.  

![Main-Menu \label{fig:MainMenu}](raw/GUI_Main_Menu.png "Main Menu")

## Datenbankschnittstelle

Die Daten werden in einem relationalem Datenbankmanagementsystem\g{}  der Firma Oracle (MySQL) gespeichert. Der Zugang wird über die vom Rechenzentrum\g{}  erhaltenen Daten erfolgen. Der Aufbau der Datenbank erfolgt über ein SQL-Skript <!-- (siehe Anlage) -->.

## Kommunikationsschnittstelle

Der Nachrichtenaustausch wird ebenso über die Datenbank realisiert. Hier wird dem Benutzer die Möglichkeit gegeben, über einen \enquote{Briefkasten} Informationen des Therapeuten zu erhalten.
