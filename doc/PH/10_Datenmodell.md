# Datenmodell

Die Modellierung des Datenmodells erfolgte mittels ERM (Entity Relationship Model)\g{} .
Aufgrund des bestehenden Zugangs des Rechenzentrums an der HTW-Dresden wurde zur Modellierung das Tool \enquote{MySQL-Workbench} der Firma Oracle benutzt.

![ERM \label{fig:ERM}](img/ERM.pdf "ERM")
