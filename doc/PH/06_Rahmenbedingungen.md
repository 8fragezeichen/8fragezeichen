# Rahmenbedingungen \label{SecRahmenbedingungen}

## Organisatorische Rahmenbedingungen

\input{06.1_RahmenbedingungenOrga}

## Technische Rahmenbedingungen

\input{06.2_RahmenbedingungenTechnische}

## Rechtliche Rahmenbedingungen

\input{06.3_RahmenbedingungenRechtlich}

