<!--

definition list in markdown (extra) supported by pandoc

Term
:	Definition

-->

**Abstrakter Anwendungsfall**
:   Ein Anwendungsfall (engl. use case) bündelt alle möglichen Szenarien, die eintreten können, wenn ein Akteur (z.B. Patient, Therapeut) versucht, mit Hilfe des betrachteten Systems ein bestimmtes fachliches Ziel zu erreichen. Er beschreibt, was inhaltlich beim Versuch der Zielerreichung passieren kann und abstrahiert von konkreten technischen Lösungen.(\cite{oV:wiki1})

**Aktivität**
:   Eine Einheit der Aktivitätsarten, z.B. Kreislauftraining, Fettabbau, Rückenschule, Stärkung der Beweglichkeit, Kräftigung, Yoga.

**ERM**
:	Entity Relationship Model - Eine modellhafte Darstellung der Beziehungen zwischen einzelnen Entitäten, üblicherweise bezogen auf die Datenstruktur oder den Aufbau einer Datenbank.

**Exceptionhandling**
:   syn. Ausnahmebehandlung. Eine Ausnahme oder Ausnahmesituation (englisch exception) bezeichnet das Verfahren, Informationen über bestimmte Programmzustände – meistens Fehlerzustände – an andere Programmebenen zur Weiterbehandlung weiterzureichen.  

**GUI**
:   Grafische Benutzeroberfläche oder auch grafische Benutzerschnittstelle (Abk. GUI von englisch graphical user interface)

**Interoperabilität**
:   Interoperabilität ist die Fähigkeit des Softwaresystems, mit anderen gegenwärtigen oder zukünftigen Softwaresystemen ohne Einschränkungen hinsichtlich Zugriff oder Implementierung zusammenzuarbeiten bzw. zu interagieren.

**Kontext**
:   syn. Umfeld. Personen oder Systeme, die über Schnittstellen (siehe \autoref{Sec:Schnittstellen}) mit dem Softwaresystem interagieren.  

**Modifizierbarkeit**
:   Die Modifizierbarkeit von Software beschreibt, mit welchem Aufwand dieselbe an neue, zukünftige Anforderungen angepasst werden kann. Die Modifizierbarkeit bezieht sich dabei auf die Architektur der Software, deren Design oder bestimmte Implementierungen.

**Patientendaten**
:   Alle personengebundenen Daten bezüglich des Patienten, vgl. \autoref{Subsec:Datenstruktur} 

**PDF**
:	Portables Dokumentenformat: Ein Dateityp der speziell für Drucksachen auf einem bestimmten (Papier-)Format entwickelt wurde. Zeichnet sich in vielen Implementierungen durch starke Möglichkeiten zur Rechteeinschränkung für Nutzer und weitgehende Standardisierung -- getrieben vom Hersteller der Referenz-Implementierung *Adobe* -- aus.

<!--
**PNG**
:	Portable Network Graphics: Ein Dateityp der speziell für die Anforderungen im Internet und die Bildschirmanzeige entwickelt wurde. Kaum geeignet für Drucksachen.
-->

**Rechenzentrum**
:   Bezeichnung für das Rechenzentrum der Hochschule für Technik und Wirtschaft Dresden.

**Relationale Datenbank** 
:   Eine relationale Datenbank ist eine Sammlung von Datenelementen, die als ein Satz formal beschriebener Tabellen organisiert sind. 
    
**Rolle**
:   Nutzer des Systems nehmen die Rolle des Patienten oder des Therapeuten ein.

**Schnittstelle**
:   syn. Verbindungsstelle. Führung des Benutzers bzw. Möglichkeit, z.B. über ein Menue Eingaben zu tätigen, Berührungspunkt zwischen Benutzer und Softwaresystem

**Skalierung**
:   In der Computergrafik und digitalen Bildbearbeitung bezeichnet Skalierung die Größenänderung eines digitalen Bildes. (\cite{oV:wiki2}) 

**SVG**
:	Scaleable Vector Graphics: Ein Dateityp für skalierbare Grafiken, basiert auf XML. Starke Eignung als Vorlage für Drucksachen.

**UML (Unified Modeling Language)**
:   Vereinheitlichte Sprache zur Modellierung, grafischen Spezifikation, Konstruktion und Dokumentation von Software-Teilen und anderen Systemen, dient der Einhaltung von Qualitätstandards mit Hilfe der Objektorientierung (\cite{Sommerville:SE}) 
