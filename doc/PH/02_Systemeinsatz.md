# Systemeinsatz

Das Software System soll, wie vom Kunden gewünscht, in den Räumen der HTW Anwendung finden und auf den Computern des Labores Z136b zum Einsatz kommen.

## Hardwareanforderungen \label{subsec:AnforderungenHardware}

Das System wird in Java entwickelt und verlangt aufgrund der Datenbankanbindung eine stabile Internetanbindung. Als Mindestanforderungen an den PC gelten: 
 
 - Prozessor: Intel Core 2 Duo 
 - Arbeitsspeicher: 1 GB 
 - Internet: 56 KB/s 
 - freier Speicherplatz: 500 MB
 - Drucker, A4
 
## Softwareanforderungen

Das System ist in erster Linie für den Einsatz auf Desktop-PCs optimiert und in Eclipse entwickelt.
Das System wird in Java realisiert. Dadurch wird Plattformunabhängigkeit gewährleistet.
Als Anforderung an den PC wird eine aktuelle Version des *Java Runtime Environment* (JRE, hier ab Version 8) benötigt.

Weiterhin muss die Peripherie betriebsbereit sein, insbesondere ist es
notwendig, dass Drucker und Internetanschluss im Betriebssystem durch
Treiber und Konfiguration zur Verfügung stehen.
Als Ausweichmöglichkeit zum Drucker wird empfohlen einen virtuellen
Drucker (zur Ausgabe von PDF-Dateien) einzurichten.

## Datensicherung

Das System nutzt eine vorhandene MySQL Datenbank zur Datensicherung. Um die fehlerfreie Nutzung zu gewährleisten, wird eine Internetverbindung benötigt.
