# Ausgangssituation

> \enquote{Es ist der Geist, der sich den Körper baut.} (Friedrich Schiller, 1759 - 1805)

**Kommunikation verbessern, Brücken bauen**, dem Körper und dem Geiste etwas Gutes tun. Die sich rasant weiterentwickelnde, vernetzte Welt hält auch im medizinischen Bereich Einzug. Doch während sich die IT-Welt immer rascher wandelt, wird noch nach Lösungen gesucht, um den direkten Patientenkontakt nicht unterbrechen zu lassen. Eine Weiterführung von Therapiemaßnahmen ist oft zu kostspielig und die Kommunikation bleibt in den meisten Fällen auf der Strecke. 

Diesem Problem, der mangelnden Kontakterhaltung und Überwachung des Therapieverlaufs, soll *\projektname{}* eine Lösung bieten.
Der Therapiesuchende kann nach der 1. Phase der Behandlung weiterhin mit dem Therapeuten in Kontakt bleiben und sich in seinen Genesungsprozess unterstützen lassen.

Das System soll eine **unterstützende Wirkung** auf den Heilungsprozess bewirken und den Austausch zwischen Patienten und Therapeuten stark vereinfachen. Es soll und kann aber kein Ersatz für direkten Kontakt mit dem Therapeuten darstellen.

Dem Anwender der Software *\projektname{}* wird eine **gut strukturierte, intuitive Benutzeroberfläche** zur Verfügung gestellt. Besonderer Augenmerk wird auf eine **einfache Bedienung** und gut geführte Nutzerdokumentation gelegt.

Zum besseren Verständnis werden Begriffe im Pflichtenheft, welche nachfolgend mit \enquote{\g{}} gekennzeichnet sind, im Glossar näher beschrieben.
