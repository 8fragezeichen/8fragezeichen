# Abnahmekriterien

Für die Abnahme des Produktes durch den Kunden werden Anforderungen festgelegt,
die erfüllt sein müssen. Darüber hinaus werden die Rahmenbedingungen geklärt, unter denen die Abnahme erfolgt.

Die Abnahmekriterien gliedern sich in folgende Punkte:  

1. Rahmenbedingungen
2. Mindestanforderungen

## Rahmenbedingungen

Die Übergabe erfolgt in den letzten Wochen des SS2016 in den Räumen der HTW-Dresden.
Zur Nutzung des Systems wird durch den Kunden eine JRE auf den Rechnern des Labors aufgespielt.

Das Rechenzentrum der HTW-Dresden stellt eine MySQL Datenbank zur Nutzung zur Verfügung.


## Mindestanforderungen

Unter Berücksichtigung der Aufgabenstellung und der entstandenen Anforderungen werden
folgende Anforderungen als Mindestanforderungen festgelegt:

* Anlegen von Aktivitätsdaten
* Verwaltung von Aktivitätsplänen
* Nutzerverwaltung
* Testdatengenerierung und das Einpflegen in die Datenbank
* Auswertung von Plandaten anzeigen

## Abnahmeschema

Um die Abnahme zu erleichtern, wird folgendes Prüfschema vorgeschlagen:


**Ausgangssituation**

>Therapeut m&ouml;chte Patientendaten anlegen

**Ereignis**

> Zugangsdaten generieren

**Erwartetes Ergebnis**

> Generierte Zugangsdaten anzeigen 

