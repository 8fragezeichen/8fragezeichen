# Qualitätsanforderungen

## Funktionalität

Das Softwaresystem \projektname{} erfüllt die unter \autoref{funktionaleAnforderungen} formulierten \enquote{Muss} Funktionalitäten. Mit den in den Satzschablonen mit \enquote{Sollte} definierten Funktionalitäten werden zusätzliche
Funktionalitäten eingebettet, die optional auch zu einem späteren Zeitpunkt implementiert werden können. Bei der Umsetzung der Funktionalitäten wird auf **Richtigkeit** und 
**Angemessenheit** geachtet. Unter Richtigkeit wird ein zuverlässiger und korrekter Ablauf der einzelnen Module verstanden und unter Angemessenheit die Konzentration auf 
die Implementierung sinnvoller Funktionalitäten von klarer Struktur. 

Durch die Interaktion zwischen Therapeuten und Ärzten ist ein Datenaustausch über den Systemkontext hinaus zu ermöglichen. Durch die Nutzung gängiger Werkzeuge und Implementierung 
kompatibler Schnittstellen wird die **Interoperabilität**\g{}  gewährleistet. Unter dem Aspekt der **Ordnungsmäßigkeit** wird die Einhaltung gesetzlicher Vorschriften und 
**Normenkonformität** verstanden. Ein wichtiger Punkt ist die **Sicherheit** des Softwaresystems \projektname{}, welche durch Exceptionhandling\g{}  und die sorgfältige Durchführung der Fehlertoleranzanalyse sowie Testphase, Hinweise zum Fehlerhandling und die Sicherstellung einfacher **Wartbarkeit** verwirklicht wird.

## Zuverlässigkeit

Das Softwaresystem \projektname{} erfüllt folgende Kriterien der **Zuverlässigkeit**:

- Die **Reife** des SW-System wird in dem Maße garantiert, indem alle \enquote{MUSS} Funktionalitäten vollständig implementiert wurden und zuverlässig ablaufen. 
- Die Minimalforderung: Jede spezifizierte Anforderung wird durch mindestens ein Testfall (Dokumentation mit Test_ID) angesprochen werden.(\cite[S.\,93]{Schneider:Abenteuer})
- Die **Fehlertoleranz** erlaubt nur die unter \autoref{SecFehlertoleranz} <!--war: Punkt 7--> als tolerierbare Fehler definierten Fehler, die mit entsprechender Handlingsbeschreibung behoben werden können.

## Benutzbarkeit

Das Softwaresystem \projektname{} erfüllt einen hohen Anspruch an **Verständlichkeit**, **Erlernbarkeit** und intuitiver **Bedienbarkeit**. Die Bedienoberfläche ist klar strukturiert und 
angemessen, die Benutzerschnittstellen werden anwendungsfreundlich und verständlich gestaltet. Die unter \autoref{SecRahmenbedingungen} <!--war: Punkt 6--> formulierten Rahmenbedingungen sichern den Kontext\g{}  ab. 

\newpage 

## Effizienz

Mit Blick auf die Performance wird insbesondere das **Verbrauchsverhalten** und **Zeitverhalten** des Softwareystems \projektname{} betrachtet. Dabei werden Datenbankabrufe und 
Funktionsabläufe unter Einbeziehung performanter Methoden gestaltet.  

## Änderbarkeit

Die **Analysierbarkeit** und **Prüfbarkeit** des Softwaresystems \projektname{} wird umgesetzt durch klare Strukturierung der Komponenten, Dokumentation und Bedienoberfläche; Kommentierung 
des Quellcodes sowie Erfüllung der Anforderungen hinsichtlich Objektorientierung, Hierarchisierung und Standardisierung. Die konsequente Kapselung der Komponenten und Schnittstellen 
sichert die **Modifizierbarkeit**\g{}  und **Erweiterbarkeit** des SW-Systems ab. Die Implementierung einzelner Komponenten erfolgt schrittweise nach erfolgreichem Testlauf und -dokumentation. 
Die **Stabilität** wird durch die Erstellung von robusten Komponenten und Schnittstellen erzielt.

## Übertragbarkeit

Die **Übertragbarkeit** beinhaltet die folgenden drei Kriterien:

- **Anpassbarkeit**
- **Installierbarkeit**
- **Austauschbarkeit**(\cite{winfwiki:QS}, \cite{Schneider:Abenteuer})

Das Softwaresystem \projektname{} könnte auf Grund seiner Gestaltung unter der Einhaltung der technischen Voraussetzungen und Rahmenbedingungen auch in anderen Therapiezentren eingesetzt 
werden.

<!-- vorschlag:

## Kennzahlen zu den Qualitätsanforderungen 

+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Kennzahlbezug                          | Eingabe          | Ausgabe             | Sollkennzahl                                                        |
+========================================+==================+=====================+=====================================================================+
| Testen-Laufzeiteffizienz               | Programm starten | Programmbereitschaft| Reine Wartezeit, ohne Bedieneingriffe                               |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Testen-Laufzeiteffizienz               | Dateneingabe     | Datenbankeintrag    | normaler PC von Doppelklick bis Arbeitsfähigkeit                    |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Testen-Speichereffizienz               | Programm laden   | Programmbereitschaft| max. belegter Hauptspeicher                                         |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Testen                                 | Testfall         | Testprotokoll       | Minimalforderung: Jede spezifizierte Anforderung min. ein Testfall  |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Entwurf-Struktur/Verständlichkeit      | Entwurf          | Module              | MVC-Strukturierung\g{}                                              |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Entwurf/Implementation-Lesbarkeit      | Quellcode        | Module              | Arbeitsschritte sind kommentiert (wie, wieso)                       |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Entwurf/Implementation-Verständlichkeit| Bezeichner       | Quellcode           | Bezeichner ausreichend lang                                         |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+
| Entwurf/Implementation-Codelänge       | Quellcode        | Module              | Jede Funktionalität ein Modul, minimaler Quellcode                  |
+----------------------------------------+------------------+---------------------+---------------------------------------------------------------------+

-->
