\input{../titlepage}

\tableofcontents

\newpage

\pagenumbering{arabic}

\input{01_Ausgangssituation} \newpage

\input{02_Systemeinsatz} \newpage

\input{03_Schnittstellen} \newpage

\input{04_FuntionaleAnforderungen} \newpage

\input{05_Qualitaetsanforderungen} \newpage

\input{06_Rahmenbedingungen} \newpage

\input{07_Fehlertoleranz} \newpage

\input{08_Dokumentationsanforderungen} \newpage

\input{09_Abnahmekriterien} \newpage

\input{10_Datenmodell} \newpage

\section*{Glossar}
\addcontentsline{toc}{section}{Glossar}

\pagenumbering{Alph}

\input{Glossar}

\newpage

\printbibliography[
	title={Literaturverzeichnis}
]

\addcontentsline{toc}{section}{Literaturverzeichnis}

