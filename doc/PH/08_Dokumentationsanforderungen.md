# Dokumentationsanforderungen

Die Dokumentation soll den Projektverlauf unterstützen und den Einsatz von Werkzeugen durch das Projektteam einüben.

## Zieldokumente

Die einzelnen Dokumente sollen möglichst barrierefrei erstellt werden und in einzelnen Dateien (je Abschnitt) organisiert werden.

| Name            | Inhalt                              |
|:----------------|:------------------------------------|
| Pflichtenheft   | Anforderungsanalyse                 |
| Entwickler-Doku | Entwurf, Implementation             |
| Handbuch        | Benutzer-Dokumentation              |
| Projektverlauf  | Grobplanung, Durchführungsverfolung |

## Formate

### Textformat

Texte werden primär mit Hilfe von Markdown angefertigt und innerhalb von [GitLab](https://gitlab.com/8fragezeichen/8fragezeichen/) abgelegt.
Für die Dokumentation zur Syntax siehe [Markdown-Projekt](https://daringfireball.net/projects/markdown/syntax).
Dies bietet folgende Vorteile:

+ Stärkere Konzentration auf Inhalt durch eingeschränkte Formatierungsoptionen
+ Geringere Abhängigkeit von Werkzeugen zur Anzeige und Bearbeitung
+ Lesbarkeit und hohe Portabilität über diverse Medien
+ Konvertierbarkeit, Rekombination und Wiederverwendbarkeit mit anderen Werkzeugen
+ Bearbeitung durch mehrere Projektbeteiligte möglich

#### Weitere Vorgaben

+ Zeichensatz: UTF8
+ Zeilenumbrüche: Linefeed (Unix)

### Bilder

Bildmaterial für die Dokumentation soll -- mit Ausnahme von Screenshots -- in einem Vektorformat (z.B. PDF\g{}, optimal: SVG\g{}) abgelegt werden, um eine problemfreie Skalierung\g{} zu ermöglichen.
