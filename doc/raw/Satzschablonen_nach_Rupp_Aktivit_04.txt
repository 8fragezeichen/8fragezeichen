------------------------------------------------------ Benutzerdaten verwalten ---------------------------------------------------------------------------

Das System muss es <UNS> ermöglichen, Therapeuten anzulegen.

Das System muss es dem Therapeuten ermöglichen, Patienten anzulegen.

Das System muss es dem Therapeuten ermöglichen, die Patientendaten zu bearbeiten.

Das System muss es dem Therapeuten ermöglichen, Patienten zu löschen.

Das System muss es dem Patienten ermöglichen, sich die über ihn gespeicherten Informationen anzuzeigen.

Das System muss es dem Therapeuten ermöglichen, sich die über seine Patienten gespeicherten Informationen anzuzeigen.

// Was ist der Unterschied zwischen Patientendaten bearbeiten und Patientendaten ändern?

Das System muss es dem Patienten ermöglichen, sein Passwort zu ändern.

Das System muss es dem Therapeuten ermöglichen, sein Passwort zu ändern.

Das System muss es dem Patienten ermöglichen, sich anzumelden.

Das System muss es dem Therapeuten ermöglichen, sich anzumelden.

Das System muss es dem Patienten ermöglichen, sich abzumelden.

Das System muss es dem Therapeuten ermöglichen, sich abzumelden.


------------------------------------------------------ Aktivitätsdaten verwalten ---------------------------------------------------------------------------

Das System muss es dem Therapeuten ermöglichen, eine Aktivität zu erstellen.

Das System muss es dem Patienten ermöglichen, sich seine Aktivitätenliste anzuzeigen.

Das System muss es dem Therapeuten ermöglichen, sich die Aktivitätenliste seiner Patienten anzuzeigen.

Das System muss es dem Patienten ermöglichen, sich mehr Informationen zu einer Aktivität anzeigen zu lassen.

Das System muss es dem Therapeuten ermöglichen, sich mehr Informationen zu einer Aktivität anzeigen zu lassen.

Das System muss es dem Patienten ermöglichen, Aktivitäten zu suchen.

Das System muss es dem Therapeuten ermöglichen, Aktivitäten zu suchen.

Das System muss es dem Therapeuten ermöglichen, eine Aktivität zu bearbeiten.

Das System muss es dem Patienten ermöglichen, eine Aktivität abzuschließen.

Das System muss es dem Therapeuten ermöglichen, einem Patient eine Aktivität zu zuweisen.

Das System soll es dem Patienten ermöglichen, sich eine Aktivität zum Plan hinzuzufügen.

Das System muss es dem Therapeuten ermöglichen, eine Aktivität zu löschen.

Das System muss es dem Therapeuten ermöglichen, einen Aktivitätsplan für einen Patienten anzulegen.

Das System muss es dem Therapeuten ermöglichen, den Aktivitätsplan eines seiner Patienten zu bearbeiten.

Das System muss es dem Therapeuten ermöglichen, den Aktivitätsplan eines seiner Patienten zu löschen.


------------------------------------------------------ Nachrichtendaten verwalten ---------------------------------------------------------------------------

Das System wird es dem Therapeuten ermöglichen, Nachrichten an seine Patienten zu senden.

Das System wird es dem Patienten ermöglichen, Nachrichten an seinen Therapeuten zu senden.

Das System wird es dem Therapeuten ermöglichen, Nachrichten zu lesen.

Das System wird es dem Patienten ermöglichen, Nachrichten zu lesen.

------------------------------------------------------ Datenauswertung verwalten ---------------------------------------------------------------------------

Das System sollte es dem Patienten ermöglichen, eine Gesamtbilanz anzuzeigen.

Das System sollte es dem Patienten ermöglichen, Kennzahlen anzuzeigen.