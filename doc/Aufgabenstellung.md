Belegarbeit "Software Engineering II": Sommersemester 2016

# Aufgabenstellung

Hochschule für Technik und Wirtschaft (FH)
Fachbereich Informatik/Mathematik
Prof. Dr.-Ing. Anna Sabine Hauptmann

> Sag' es mir und ich werde es vergessen.
> Zeige es mir und ich werde mich daran erinnern.
> Beteilige mich und ich werde es verstehen.
> (Laotse)

## Entwicklung eines Softwaresystems zur Unterstützung der Kommunikation zwischen Patienten und Therapeuten bei der Durchführung von Therapiemaßnahmen

**Aufgaben der Belegbarbeit:**

Es ist ein Softwaresystem zu erstellen, das begleitend zur Therapiedurchführung eingesetzt werden kann. Insbesondere steht die Kommunikation zwischen Patienten und Therapeuten im Mittelpunkt. (Beispielhaft könnte der Therapeut ein Physiotherapeut sein.)

Ausgehend von einem persönlichen Kontakt soll die Kommunikation zwischen beiden Personen zeitweise auch unter Anwendung dieses Softwaresystems möglich sein.

Denkbar sind Anwendungsfälle wue zum Beispiel:

- Terminvereinbarung für weitere Gespräche,
- Rückmeldung des Patienten über Anzahl der selbstständig durchgeführten Übungen mit einer Wirkungsbeschreibung,
- (weitere) Verordnungen durch den Therapeuten für die zusätzlichen, selbstständigen Übungen.

Ausgehend von diesen Beispielen sind sinnvolle Anwendungsfälle zu suchen und zu beschreiben. Wichtig sind dabei auch die jeweils geltenden Rahmenbedingungen und die Bezugnahme auf eventuell weitere Akteure.

Für die prototypische Implementierung ist nach Absprache eine Auswahl aus diesen Anwendungsfällen zu treffen. Dabei sind auch Kriterien zu finden, die eine Zuordnung erlauben hinsichtlich der Frage: Welche Anwendungsfälle sind auf diesem Weg technisch realisierbar und welche Anwendungsfälle sollten weiterhin unbedingt in direkter Kommunikation zwischen Therapeut und Patient erfolgen?

### 1. Anaöyse, Anforderungsdefinition, Pflichtenheft

Die Anforderungsdefinition für die oben dargestellte Aufgabe. Das umfasst neben der textlichen Beschreibung ausgehend vom Kontextdiagramm:

- Beschreibung der Anforderungen mit Satzschablonen (nach Chris Rupp)
- entsprechende Anwendungsfalldiagramme
- entsprcehende Aktivitätsdiagramme
- Datenstrukturdefinitionen für die Auslöser und Reaktionen
- Definition der Benutzerschnittstelle (Dialog)
- ERM, RM

### 2. Entwicklung des Sollsystems (Grob- und Feinentwurf, Implementierung, Datenbankentwicklung, Test).

Für die Implementierung können Sie *nach Absprache* eine Technologie Ihrer Wahl nutzen.

Für den Entwurf sind entsprechende Entwurfsdokumente anzufertigen.

Der Test ist während der Entwicklung als Komponenten und Integrationstest sowie als Systemtest durchzuführen. Für den Systemtest sind Testfälle, ggf. Testhilfsmittel festzulegen, Testdaten zu erstellen und Testprotokolle anzufertigen.

Für die Datenbank ist auf der Basis des Relationsmodells (ERM -> RM) ein SQL-Script zu generieren, mit dem die leeren Tabellen erzegt werden können. Diese Tabellen sind mit Testdaten zu füllen, bei Bedarf jederzeit wiederholbar.

### Entwicklungs-/Einsatzumgebung

- Entwicklungswerkzeug: nch Absprache wählbar
- Einsatzumgebung: Siehe Labor Z136b

### Arbeitsetappen, Ergebnisse, Termine:

1. Analyse -&gt; Pflichtenheft
   Abgabetermine Ende 17. KW
2. Entwurf/Implementation, Inbetriebnahme
   - Entwicklerdokumentation
   - Testdokumentation Testplan, Testfälle, Testdaten, Testprotokolle
   - Benutzerdokumentation
   - mindestens vier Protokolle von Arbeitsgruppenbesprechungen
   - ggf. Pflichtenheftänderungen
   Abgabetermin: Ende des Semesters
3. Übergabe an den Kunden
   im Rahmen der Prösentation/Verteidigung, Termin nach Vereinbarung innerhalb der Prüfungsphase (ggf. nach rechtzeitiger Absprache in den letzten drei Semesterwochen)

### Arbeitsorganisation:

Die Belegarbeit ist als Gruppenarbeit mit individuell festgelegten Verantwortlichkeiten für den Gesamtzeitraum anzufertigen. (Gruppenstärke 6 bis 8 Gruppenmitglieder)
Arbeitsgruppenberatungen sind regelmäßig durchzuführen.

### Bewertungskriterien:

- Organisation und Umsetzung der Gruppenarbeit
- Wahrnehmung der prsönlichen Verantwortung
- Qualität der Zwischen- und Endprodukte

Die Bewertung der Belegarbeit erfolgt in zwei Teilen:
* Teil 1: Gruppennote (Pflichtenheft, Produkt, Dokumentation, Gruppenarbeit)
* Teil 2: individuelle Note im Rahmen der Präsentation/Verteidigung (je nach Verantwortungsbereich)

Ich wünsche Ihnen viel Erfolg, Freude bei der Bearbeitung und viele für Sie nutzbringende Erkenntnisse.
