## Kann man mehrere Accounts mit GitLab nutzen?

tl;dr: Ja.

### auf Weboberfläche

Grafisch mit unterschiedlichen Browser-Sessions.

### mit der CLI

Ja, hier zeigt GitLab scheinbar leichte Abweichungen von z.B. CGit oder GitHub.
s.a.: [doc.gitlab](http://doc.gitlab.com/ce/ssh/README.html), [so](https://stackoverflow.com/questions/29775626/ssh-config-with-multiple-keys-for-multiple-gitlab-user-accounts)

Ich gehe hier davon aus, dass Authentisierung mittels Public-Key genutzt wird, weil dies komfortabler ist als bei jedem Schirtt das Kennwort und ggf. zusätzlich den Nutzernamen einzugeben.

ssh-Konfiguration, z.B. in ``~/.ssh/config``
~~~~~
Host gitlab.com
	User {username}
	HostName gitlab.com
	IdentityFile {fully-qualified-filepath}
	IdentitiesOnly yes
~~~~~

Setzen der remote-url für das target origin mittels CLI:
~~~~~
git remote set-url origin git@gitlab.com:8fragezeichen/8fragezeichen.git
~~~~~

bzw. Ändern der Konfigurationsdatei ``.git/config``
~~~~~
[remote "origin"]
	url = git@gitlab.com:8fragezeichen/8fragezeichen.git
~~~~~

Test mit ``ssh -vT git@gitlab.com`` gibt am Ende aus welcher Nutzer erkannt wurde.
Andere Repositories (mit ggf. anderen Accounts und Keys) müssen entsprechend konfiguriert werden.

## Wie wird eine Datei ohne CLI client via GitLab hochgeladen?

Der CLI client hat als Referenz-Implementierung die vollständigste Umsetzung.
Natürlich besteht die Option einen [grafischen Client](https://git-scm.com/downloads/guis) zu verwenden.
U.a. wird [git-GUI](https://git-scm.com/docs/git-gui) mitgeliefert.
Weiterhin haben wir dank GitLab eine Weboberfläche.

### Mit der Weboberfläche

#### Klartext

1. Auf den Bereich "Files" wechseln
   + Wenn der noch nicht da ist: Formular für Neue Datei aufrufen
     * Auf die [Projektseite](https://gitlab.com/8fragezeichen/8fragezeichen) wechseln 
     * Das Plus-Zeichen neben der Clone-URL wählen
     * Im Dropdown "New file" wählen
2. Kopf "File Name" mit Pfad/Dateiname befüllen
3. Text eingeben oder aus Zwischenablage etc. einfügen
4. Noch "Commit Message" mit Kurzbeschreiung befüllen, ggf. weitere Optionen wie Branch auswählen
5. Datei speichern mit "Commit Changes"

#### Binärdateien

Man kann zunächst die Alternative eines Klartext-Formates prüfen, weil so Änderungen besser verfolgt werden können.
Das funktioniert u.a. bei Office Dokumenten: z.B. FODT statt ODT. Viele Formate sind heute tatsächlich gezipptes XML das wiederum in großen Teilen als Klartext übermittelt werden kann.

Oft ist das aber ein unverhältnismäßiger Aufwand. Dem wird hier ein anderer gegenüber gestellt, um das Prinzip von `git` "Klartext versionieren" etwas Gewicht zu verteilen.

1. Beginn wie bei Klartext
2. aber statt "text" neben dem Dateinamen "base64" wählen
3. und die Datei als Base64-Text einfügen (via Kommando `base64` - s.a. [man base64](http://linux.die.net/man/1/base64) - oder z.B. mit http://base64converter.com/ ermittelbar)
4. Abschließen wie bei Klartext
