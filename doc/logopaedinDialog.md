## Dialog mit einer Logopädin

Logopädie Raum Radebeul, mit Frau Cynthia Wolf:

* Einfaches UI / Bedienung (zugänglich)
* „Druck“ Option Schnittstelle
* Kein Messenger zwischen Therapeut & Patient -> zu Aufwändig für    Therapeuten
* Aufbauende Maßnahmen / Gewichtung angeben -> Bsp. Stimmstörung (Hormonell / Funktionell als Kategorie angeben)
* Zähler für Anzahl der therapeutischen Einheiten nach Heilmittelkatalog, damit Patient rechtzeitig neues Rezept beantragen kann (Gewährleistung lückenloser Therapie)
* Passwort Pflicht für Datenschatz, direkter Verkauf an Praxen
* Notfallnummer für Therapeuten -> Hotline
* Diagnose / Grob- & Feinziel angeben können
* Einzugsgebiet -5km zumutbar
* Kurzbeschreibung / Komplettbeschreibung … Häufigkeit, Zeit, Wiederholung, Abhaken (auch mit Zeitstempel für Kontrolle)
Erinnerung -> Kalender -> Wecker -> Benachrichtigung
* Motivation
* BILDER! Steigerung um Übungen schwieriger zu machen
* Praktik nach „…“ -> Infofenster
* Anzahl der Übungen etc. angeben, auch um Überlastung auszuschließen
* Kontaktfunktion zwischen Therapeuten, die gleiche Patienten haben -> * Kommentare / Messenger, Kontaktdaten, wer sind überhaupt Ansprechpartner, also andere Therapeuten
* Datenpakete mit Anleitungen und Bildmaterial anbieten / verkaufen, damit Therapeut nicht selber anfangen muss Bilder zu machen / Anleitungen zu schreiben
* Automatisches Update – Wissen etc. entwickelt sich weiter
* Schriftgröße anpassbar für ältere Mitmenschen
* Stetige „Zusammenarbeit“ mit Therapeuten für Feedback
* Soll Arbeit des Therapeuten erleichtern! Zeitersparnis als der Schlüssel!

