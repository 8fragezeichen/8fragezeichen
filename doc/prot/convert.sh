#!/bin/sh
pwd=$(pwd)
twd="$(echo $pwd|rev|cut -d'/' -f1|rev)"
if [ "${twd}" = "PH" ]; then	DOCNAME=Pflichtenheft ; fi
if [ "${twd}" = "RFC" ]; then	DOCNAME=Entwicklerdokumentation ; fi
if [ "${twd}" = "MAN" ]; then	DOCNAME=Benutzerhandbuch ; fi
if [ "${twd}" = "CR" ]; then	DOCNAME=Änderungensnachweise ; fi
if [ "${twd}" = "prot" ]; then  DOCNAME=Protokolle ; fi
if [ "${twd}" = "PD" ]; then	DOCNAME=Projektdokumentation ; fi
if [ "${DOCNAME}" = "" ]; then
	echo ERROR: no doc name
	exit 1
fi
export DOCNAME
wrapper=00_Manteldokument
inv=inventory
md2tex=pandoc
sed=sed
tex2pdf=pdflatex
bib2tex=biber
doc=".."
img=${doc}/img
pub=${doc}/pub
raw=${doc}/raw
# prepare directories for usage in document
if [ ! -h raw ]; then
	ln -s ${raw} raw
fi
if [ ! -h img ]; then
	ln -s ${img} img
fi
cd ./img/
if [ $? -eq 0 ]; then
	./convert-svg2pdf.sh
	cd ${pwd}
fi
cp ${doc}/${wrapper}.tex ./
#cp ${doc}/${inv}.tex ./
# create index for protocols
if [ "${twd}" = "prot" ]; then
	cp 00_Gliederung.md 00_Gliederung.bak
	for p in $(ls Prot_*.pdf); do
		t=$( echo ${p} | rev | cut -d'.' -f2- | rev | cut -d'_' -f2 )
		y=$( echo ${t}|cut -d'-' -f1)
		m=$( echo ${t}|cut -d'-' -f2)
		d=$( echo ${t}|cut -d'-' -f3)
		dat=${d}'.'${m}'.'${y}
		t='Protokoll '${dat}
		echo "\\includepdf[pages=-,addtotoc={1,section,1,"${t}","${dat}"}]{${p}}" >> 00_Gliederung.md
	done
fi
x="PersonenAufgabenbereiche"
${md2tex} -f markdown -t latex -o ./${x}.tex ${doc}/${x}.md
for i in $(ls *.md); do
	x=$(echo "${i}" | rev | cut -d"." -f2- | rev)
	${md2tex} -f markdown -t latex -o ${x}.tex ${x}.md
	${sed} -i 's/\includegraphics/\scalegraphics/g' ${x}.tex
done
#~ ${tex2pdf} ${wrapper}.tex && ${bib2tex} ${wrapper} && ${tex2pdf} ${wrapper}.tex && ${tex2pdf} ${wrapper}.tex
${tex2pdf} ${wrapper}.tex 
if [ 0 -lt $(ls ../*.bib 2>/dev/null|wc -l) ]
then
	${bib2tex} ${wrapper}
	${tex2pdf} ${wrapper}.tex
fi
${tex2pdf} ${wrapper}.tex
# join pdfs
#pdfjoin -o j.pdf ${wrapper}.pdf $(ls P*.pdf)
if [ "${twd}" = "prot" ]; then
	mv 00_Gliederung.bak 00_Gliederung.md
fi
datum=$( date +"%y%m%d-%H%M" )
mkdir -p ${pub}

mv ${wrapper}.pdf ${pub}/00_${DOCNAME}-draft${datum}.pdf && xdg-open ${pub}/00_${DOCNAME}-draft${datum}.pdf

rm -f *.aux *.bbl *.bcf *.blg *.blg *.log *.mtc *.mtc0 *.xml *.toc
rm -f $(ls *.tex )
rm -f ${img}/*.pdf
