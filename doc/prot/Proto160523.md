Hier was ich mitgetippt habe zum Zusammenfahren im Protokoll

* Präsentation - Eric
  + Vorbereiten
  + Vorschlag Flipchart statt Powerpoint
  +
* Tests - Neda
  + Layout (logout-button, springende symbole)
  + Usabilty (erkennbarkeit, optionale angaben, frustrations tolleranz)
  + Verifikation (eingabemöglichkeiten beschränken, buttons
ausgrauen/einfärben)
  + System-Feddback f. User
* Diskussion
  + Erkannte Fehler/Mängel werden anerkannt
  + Dokumentation ja, Korrektur nur bedingt
  + Mehr Doku im Git als Issue, mit Zuordnung zum Meilenstein
  + Testplan wie im PH ausführen
  + Änderungen zum PH (auch Rechtschreibfehler) als
[ChangeRequest](doc/form-ChangeRequest.md) (Bsp. Formular, QM reduziert
ggf.)
* Doku
  + Entwickler-Doku - JavaDoc, Rest wie Pflichtenheft
  + Anwender-Doku - wie Pflichtenheft
  + Terminierung in Meilensteinen noch zu setzen
