
* Vorstellung Stand
* Erläuterung der Treffen
  + für Präsi nutzen => Stakeholder teilw. befragt
  - Was meint "Benutzerreundlichkeit"?
  + Angst ummünzen in Chance (Zeitknappheit bei Medizienern)
    * Benutzerdoku: Keine falschen Erwartungen wecken
* Satzschablonen gezeigt
  + Soll/Kann ordentlich verwenden
* QM: 5 Punkte von Uta
* ERM
  + Erweiterungsmöglichkeiten sicherstellen
  - Nachrichten fehlen
  - Zuordnung (Terapeuth/Patient) Aktivität sicherstellen
  - Kreisform => Sternform! (Plan ist Zuordnungstabelle)
  - Plan-Balupause fehlt
  + Begründungen in Entwickler-Doku
* Eigenverantwortlichkeiten
* Rechtl. Rahmenbedingungen
