### Therapeutenpasswort ändern

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue11}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Herz ![ico](raw/ico_Herzsymbol.png) klicken, um auf die Home-Seite zu gelangen.

![Home GUI \label{fig:Home3}](raw/T_Home.png "Home GUI")

\newpage

Klicken Sie anschließend auf den Button \enquote{Password Ändern}.
Mit der Aktion öffnet sich ein Fenster, in dem Sie Ihre Passwort ändern können.

![Hauptmenü GUI \label{fig:Hauptmenue12}](raw/T_Passwort_aendern_1.png "Hauptmenü GUI - Passwort ändern")

Nach Eingabe des alten Kennworts und zweimaliger Eingabe des neuen Kennworts können Sie durch einen Klick auf den Button \enquote{Ändern}
das neue Kennwort setzen. (Achten Sie darauf, dass das Kennwort mehr als 6 Zeichen enthält.)

![Kennwort ändern \label{fig:KennwortAendern3}](raw/T_Passwort_aendern_2.png "Kennwort ändern")

\newpage

### Patienten anlegen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue13}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit der Person ![ico](raw/ico_PatientVerwalten.png) klicken, um in die Patientenverwaltung zu gelangen.

![Patientenverwaltung GUI \label{fig:Patientenverwaltung1}](raw/T_Patienten_verwalten.png "Patientenverwaltung GUI")

\newpage

Klicken Sie dort auf \enquote{Patienten anlegen} ![ico](raw/ico_PatientAnlegen.png "Patient anlegen") .
Es erscheint die Eingabemaske.

![Patienten anlegen \label{fig:Eingabemaske}](raw/T_Patienten_anlegen_1.png "Eingabemaske")

Nachdem Sie alle Daten eingegeben haben, betätigen Sie den Button \enquote{anlegen}.

![Patienten anlegen \label{fig:Eingabemaske1}](raw/T_Patienten_anlegen_3.png "Bestätigung der ausgefüllten Eingabemaske")

Es öffnet sich ein Drucken-Dialog. Wählen Sie Ihren Drucker aus und klicken sie auf \enquote{OK}, um die Logindaten für den Nutzer zu drucken.  Falls Sie einen
PDF-Reader installiert haben, können Sie Ihre Benutzerdaten alternativ als PDF erzeugen und speichern.

\newpage

![Patienten anlegen \label{fig:Patientendatenblatt}](raw/Pmamus.pdf "Patientendatenblatt")

\newpage

### Patienten bearbeiten

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue14}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit der Person klicken, um in die Patientenverwaltung ![ico](raw/ico_PatientVerwalten.png) zu gelangen.

![Patientenverwaltung GUI \label{fig:Patientenverwaltung2}](raw/T_Patienten_verwalten.png "Patientenverwaltung GUI")

\newpage

Klicken Sie dort auf \enquote{Patienten bearbeiten} ![ico](raw/ico_PatientenAnzeigen.png).

![Patienten bearbeiten \label{fig:EditPatient0}](raw/T_Patienten_bearbeiten_1.png "Patientendaten bearbeiten")

Wählen Sie nun in der Tabelle den Patienten, den Sie bearbeiten wollen aus und klicken auf den Button mit dem Bleistift.

![Patienten bearbeiten \label{fig:EditPatient1}](raw/T_Patienten_bearbeiten_2.png "Patientendaten bearbeiten")

\newpage

Mit der Aktion öffnet sich das Fenster mit der Ansicht der vorliegenden Patientendaten, in dem Sie die gewünschten Änderungen vornehmen können.
Wenn Sie alle Änderungen vorgenommen haben, drücken auf auf den Button \enquote{Ändern}.

![Patienten bearbeiten \label{fig:EditPatient2}](raw/T_Patienten_bearbeiten_3.png "Patientendaten bearbeiten")

Der Dialog \enquote{Patientendaten wirklich ändern} überprüft die willentliche Eingabe der Änderung.

![Abfragedialog \label{fig:EditPatient3}](raw/T_Patienten_bearbeiten_4.png "Abfragedialog")

Bestätigen Sie mit \enquote{OK} werden Ihre Eingaben übernommen.

\newpage

### Patienten löschen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue15}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit der Person ![ico](raw/ico_PatientVerwalten.png) klicken, um in die Patientenverwaltung zu gelangen.

![Patientenverwaltung GUI \label{fig:Patientenverwaltung3}](raw/T_Patienten_verwalten.png "Patientenverwaltung GUI")

\newpage

Klicken Sie dort auf \enquote{Patienten bearbeiten}. Wählen Sie in der Tabelle den Patienten, den sie bearbeiten wollen aus und klicken auf den Löschbutton ![ico](raw/ico_Loeschen.png) .
Bestätigen Sie die erscheinende Abfrage mit einem Klick auf \enquote{OK}.

![Patienten löschen \label{fig:DeletePatient}](raw/T_Patienten_loeschen_2.png "Abfragedialog")

\newpage

### Patientenkennzahlen anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue16}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit der Person ![ico](raw/ico_PatientVerwalten.png) klicken, um in die Patientenverwaltung zu gelangen.

![Patientenverwaltung GUI \label{fig:Patientenverwaltung4}](raw/T_Patienten_verwalten.png "Patientenverwaltung GUI")

\newpage

Klicken Sie dort auf \enquote{Patienten Monitoring}. Für die Anzeige wählen Sie im Drop-Down-Menü einen Patienten aus.

![Patientenmonitoring \label{fig:Patientenmonitoring}](raw/T_Patienten_Monitoring_2.png "Patientenmonitoring")

\newpage

### Aktivität anlegen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue17}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten1}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivität anlegen} und füllen das Formular aus.

![Aktivität anlegen GUI \label{fig:Aktivitaetanlegen1}](raw/T_Aktivitaeten_anlegen_1.png "Aktivität anlegen GUI")

Zum Abschluss klicken Sie auf \enquote{anlegen}.

![Aktivität anlegen GUI \label{fig:Aktivitaetanlegen2}](raw/T_Aktivitaeten_anlegen_2.png "Aktivität anlegen GUI")

Den Dialog \enquote{Aktivität erfolgreich angelegt} können Sie mit \enquote{OK} bestätigen.

\newpage

### Aktivität bearbeiten

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue18}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten2}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivität bearbeiten}.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten1}](raw/T_Aktivitaeten_bearbeiten_1.png "Aktivitäten bearbeiten GUI")

Um eine Aktivität zu bearbeiten, müssen Sie die entsprechende Aktivität in der Tabelle durch einen Klick darauf auswählen
und auf den Bleistift ![ico](raw/ico_bearbeiten.png) klicken.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten2}](raw/T_Aktivitaeten_bearbeiten_2.png "Aktivitäten bearbeiten GUI")

\newpage

Nun können Sie die gewünschten Daten ändern. Wenn Sie die Änderungen vorgenommen haben, müssen Sie diese noch durch einen Klick auf
\enquote{Ändern} übernehmen.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten3}](raw/T_Aktivitaeten_bearbeiten_3.png "Aktivitäten bearbeiten GUI")

Es erscheint der Bestätigungsdialog \enquote{Aktivität wirklich ändern?}. Soll die Änderung tatsächlich übernommen werden, bestätigen Sie dies mit Klick auf \enquote{OK}.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten4}](raw/T_Aktivitaeten_bearbeiten_4.png "Aktivitäten bearbeiten GUI")

\newpage

### Aktivität löschen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue19}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten3}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivität bearbeiten}.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten5}](raw/T_Aktivitaeten_bearbeiten_1.png "Aktivitäten bearbeiten GUI")

Um eine Aktivität zu löschen, müssen Sie die entsprechende Aktivität in der Tabelle durch einen Klick darauf auswählen
und auf den das Kreuz ![ico](raw/ico_Loeschen.png) klicken.

![Aktivitäten bearbeiten GUI \label{fig:Aktivitaetenbearbeiten6}](raw/T_Aktivitaeten_bearbeiten_2.png "Aktivitäten bearbeiten GUI")

Soll die Aktivität tatsächlich gelöscht werden, bestätigen Sie den Dialog \enquote{Aktivität wirklich löschen?} mit einem Klick auf \enquote{OK}.

\newpage

### Aktivitätsplan anlegen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue20}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten4}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivitätenplan} ![ico](raw/ico_AktivitatenPlan.png) .

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan1}](raw/T_Aktivitaeten_Plan_anlegen_1.png "Aktivitätenplan GUI")

Um einen Aktivitätsplan für einen Patienten anzulegen, müssen Sie einen Patienten und eine entsprechende Aktivität sowie ein Start- und ein Enddatum auswählen.

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan2}](raw/T_Aktivitaeten_Plan_anlegen_2.png "Aktivitätenplan GUI")

\newpage

Klicken Sie nun auf den Butten \enquote{hinzufügen}. Soll der Aktivitätsplan tatsächlich angelegt werden, bestätigen Sie den Abfragedialog \enquote{Wirklich hinzufügen?} durch ein Klick auf \enquote{OK}.

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan3}](raw/T_Aktivitaeten_Plan_anlegen_3.png "Aktivitätenplan GUI")

Dies können Sie so lange wiederholen, bis alle gewünschten Aktivitäten im Plan vorhanden sind.

\newpage

### Aktivität suchen in Aktivitätsplan

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue21}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten5}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivitätenplan} ![ico](raw/ico_AktivitatenPlan.png) .

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan4}](raw/T_Aktivitaeten_Plan_anlegen_1.png "Aktivitätenplan GUI")

Klicken Sie auf den Button \enquote{Suchen}. Mit der Eingabe des Aktivitätsnamen im Suchfenster, wird die gewünschte Aktivität selektiert und die Tabelle automatisch aktualisiert.

![Aktivität suchen GUI \label{fig:Aktivitaetsuchen}](raw/T_Aktivitaeten_suchen.png "Aktivität suchen GUI")

\newpage

### Aktivität aus Aktivitätsplan löschen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue22}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten6}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivitätenplan} ![ico](raw/ico_AktivitatenPlan.png) .

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan5}](raw/T_Aktivitaeten_Plan_anlegen_1.png "Aktivitätenplan GUI")

Wählen sie den Patienten aus, dessen Aktivitätenplan Sie ändern möchten und öffnen Sie mit einem Klick auf \enquote{Gesetzte Aktivitäten} den aktuellen Aktivitätsplan.

![Aktivitätenplan bearbeiten GUI \label{fig:Aktivitaetenplananzeigen}](raw/T_Aktivitaeten_Plan_loeschen_1.png "Aktivitätenplan bearbeiten GUI")

\newpage

Wählen Sie die zu löschende Aktivität durch einen Klick auf die entsprechende Zeile der Tabelle aus.

![Aktivitätenplan bearbeiten GUI \label{fig:Aktivitaetenplananzeigen2}](raw/T_Aktivitaeten_Plan_loeschen_2.png "Aktivitätenplan bearbeiten GUI")

Um die Aktivität aus dem Aktivitätsplan zu löschen, müssen Sie noch auf den Button \enquote{Löschen} klicken. Soll die Aktivität tatsächlich aus dem Aktivitätsplan gelöscht werden, bestätigen Sie den Abfragedialog \enquote{Aktivität gelöscht} mit \enquote{OK}.

\newpage

### Aktivitätsplan löschen

Um den Aktivitätsplan für einen Patienten zu löschen, müssen Sie alle Aktivitäten für den Patienten aus den Plan löschen. (siehe \enquote{Aktivität aus Aktivitätsplan löschen})

### Aktivitätsinformation anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue23}](raw/T_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um in die Aktivitätsverwaltung zu gelangen.

![Aktivitätenverwaltung GUI \label{fig:Aktivitaetenverwalten7}](raw/T_Aktivitaeten.png "Aktivitätenverwaltung GUI")

\newpage

Klicken Sie auf \enquote{Aktivitätenplan} ![ico](raw/ico_AktivitatenPlan.png) .

![Aktivitätenplan GUI \label{fig:Aktivitaetenplan6}](raw/T_Aktivitaeten_Plan_anlegen_1.png "Aktivitätenplan GUI")

Wählen sie aus dem Drop-Down-Menü \enquote{Aktivität} die gewünschte Aktivität aus und öffnen Sie die Beschreibung durch einen Klick auf \enquote{Aktivitätsinformationen}.

![Aktivitätsinformationen GUI \label{fig:Aktivitaetenplan7}](raw/T_Aktivitaeten_Informationen_anzeigen.png "Aktivitätsinformationen GUI")
