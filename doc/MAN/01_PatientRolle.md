
### Das Hauptmenü

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue2}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen sie nun auf das Symbol mit dem Herz ![ico](raw/ico_Herzsymbol.png) klicken, um auf die Home-Seite zu gelangen.

![Home GUI \label{fig:Home1}](raw/P_Hauptmenue_2.png "Home GUI")

\newpage


### Persönliche Daten ändern

Ausgehend vom Haptmenü klicken Sie nun auf den Button \enquote{Meine Daten ändern}.
Nun öffnet sich ein Fenster, in dem Sie Ihre personenbezogenen Daten ändern können. 
Nach vorgenommener Änderung müssen Sie Ihre Änderungen mit einem Klick auf den Button \enquote{Speichern} bestätigen.

![Daten Ändern \label{fig:DatenGUI}](raw/P_Daten_aendern.png "Daten Ändern")

Danach schließt sich das Eingabefenster wieder.

\newpage


### Patientenpasswort ändern

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue3}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit dem Herz ![ico](raw/ico_Herzsymbol.png) klicken, um auf die Home-Seite zu gelangen.
Klicken Sie nun auf den Button \enquote{Password ändern}.

![Home GUI \label{fig:Home2}](raw/P_Hauptmenue_2.png "Home GUI")

\newpage

Nun öffnet sich ein Fenster, in dem Sie ihre Passwort ändern können.

![Kennwort ändern \label{fig:KennwortAendern1}](raw/P_Kennwort_aendern.png "Kennwort ändern")

Nach Eingabe des alten Kennworts und zweimaliger Eingabe des neuen Kennworts können Sie durch einen Klick auf den Button \enquote{Ändern}
das neue Kennwort setzen.

![Kennwort ändern \label{fig:KennwortAendern2}](raw/P_Kennwort_aendern_2.png "Kennwort ändern")

Danach schließt sich das Eingabefenster wieder.

\newpage

### Aktivitätsplan anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue4}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit dem gekreuzten Lineal und Bleistift klicken ![ico](raw/ico_AktivitatenAnzeigen.png), um auf die Aktivitätsseite zu gelangen.

![Aktivitäten GUI \label{fig:AktivitaetenGUI1}](raw/P_Aktivitaeten_offen.png "Aktivitäten GUI")

Auf dieser Seite können Sie durch Klick auf \enquote{To-Do} Ihre offenen Aktivitäten und durch Klick auf \enquote{Abgeschlossen} Ihre abgeschlossen Aktivitäten sehen 
(\enquote{Zu erledigende Aktivitäten anzeigen} und \enquote{Abgeschlossene Aktivitäten anzeigen}).

\newpage

### Zu erledigende Aktivitäten anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue5}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen sie nun auf das Symbol mit dem gekreuzten Lineal und Bleistift klicken, um auf die Aktivitätsseite zu gelangen ![ico](raw/ico_AktivitatenAnzeigen.png) .
Auf dieser Seite müssen sie nun auf den Reiter \enquote{To-Do} klicken.

![offene Aktivitäten \label{fig:AktivitaetenGUI2}](raw/P_Aktivitaeten_offen.png "offene Aktivitäten")

\newpage

### Abgeschlossene Aktivitäten anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue6}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit dem gekreuzten Lineal und Bleistift klicken, um auf die Aktivitätsseite zu gelangen ![ico](raw/ico_AktivitatenAnzeigen.png) .

![Aktivitäten GUI \label{fig:AktivitaetenGUI3}](raw/P_Aktivitaeten_offen.png "Aktivitäten GUI")

\newpage

Auf dieser Seite klicken Sie auf den Reiter \enquote{Abgeschlossen}.

![abgeschlossene Aktivitäten \label{fig:AktivitaetenGUI4}](raw/P_Aktivitaeten_abgeschlossen.png "abgeschlossene Aktivitäten")

\newpage

### Aktivität abschließen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue7}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie auf das Symbol mit dem gekreuzten Lineal und Bleistift klicken, um auf die Aktivitätsseite zu gelangen ![ico](raw/ico_AktivitatenAnzeigen.png) .

![Aktivitäten GUI \label{fig:AktivitaetenGUI5}](raw/P_Aktivitaeten_offen.png "Aktivitäten GUI")

\newpage

Nun können Sie in der Tabelle die gewünschte Aktivität auswählen (Klick auf die Aktivität) und diese mit einem Klick auf den Button \enquote{Abschließen} als erledigt markieren.

![Aktivität abschließen \label{fig:Aktivitaetabschliessen}](raw/P_Aktivitaeten_offen_details.png "Aktivität abschließen")

\newpage

### Nachrichten anzeigen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue8}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit dem Brief ![ico](raw/ico_Nachrichten.png) klicken, um auf die Nachrichtenseite zu gelangen.
Dort könne Sie Ihre Nachrichten und die Antworten darauf im Kasten unter der Überschrift \enquote{Deine Nachrichten} lesen.

![Nachrichten GUI \label{fig:messagesGUI}](raw/P_Nachrichten.png "Nachrichten GUI")

\newpage

### Nachrichten verfassen

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

![Hauptmenü GUI \label{fig:Hauptmenue9}](raw/P_Hauptmenue.png "Hauptmenü GUI")

Im Hauptmenü müssen Sie nun auf das Symbol mit dem Brief ![ico](raw/ico_AktivitatenAnzeigen.png) klicken, um auf die Nachrichtenseite zu gelangen.

![Nachrichten GUI \label{fig:messagesGUI2}](raw/P_Nachrichten.png "Nachrichten GUI")

Dort können Sie im Eingabefeld unter der Box \enquote{Deine Nachrichten} Ihre Nachricht an den Therapeuten eingeben.
Durch einen Klick auf den Button \enquote{senden} werden diese an den Therapeuten abgeschickt.
