\input{../titlepage}

\tableofcontents

\newpage

\pagenumbering{arabic}

# Anforderungen

Die Anforderungen an Hard- und Software werden von den meisten
handelsüblichen Systemen erfüllt. Die hier folgend genannten
Aufzählungen sind zugesichert. Die Verwendung auf weiteren Systemen
erfolgt in eigener Regie.

## Hardwareanforderungen

Ein aktueller PC oder Mac auf dem Java ab Version 1.8 installiert werden
kann.

Für den Druck der Passwortbriefe wird ein Drucker empfohlen.

## Softwareanforderungen

**Betriebssystem**

Linux, MacOS (OS X oder aktueller) oder Windows (Windows 7 oder aktueller)

Alternativ zu einem Drucker kann auch ein virtueller Drucker installiert
und konfiguriert werden.

**Sonstiges**

Java inklusive JavaFX Version 1.8 oder aktueller 

Bei Einsatz eines Druckers, Druckertreiber für ihre Hardware und
Konfiguration im Betriebssystem.

# Installation

Die Anwendung besitzt kein Setup. Sie können sie an einen beliebigen Ort
Ihrer Wahl kopieren und von dort aus starten. 

\newpage

# Anwendungsfälle nach Rollen

Die Benutzer haben unterschiedliche Rechte und werden durch ihre Rollen
klassifiziert.

## Rollenunabhängige Anwednungsfälle

Diese Funktionen sind unabhängig von der Rolle als Patient oder Therapeut.

### Login \label{NutzerLogin}

Nachdem die Anwendung gestartet worden ist, sehen Sie das Loginfenster.

![Login GUI \label{fig:LoginGUI}](raw/Loginscreen.png "Login GUI")

Dort tragen Sie Ihren Benutzernamen und Ihr Kennwort ein, welche Sie von der Betreibergesellschaft *8fragezeichen* erhalten haben und klicken Sie auf den Butten \enquote{einloggen}.

\newpage

### Logout \label{NutzerLogout}

Falls Sie das Hauptmenü noch nicht offen haben, öffnen Sie es durch einen Klick auf das Menüsymbol ![ico](raw/ico_MenueButton.png) .

Dort finden Sie ganz unten die Schaltfläche \enquote{Logout}. Durch einen Klick auf diese, melden Sie sich von der Anwendung ab.
Danach können Sie die Anwendung durch einen Klick auf Schließen beenden. ![ico](raw/ico_schliessen.png)

\newpage


## In der Rolle des Patienten

\input{01_PatientRolle} \newpage

## In der Rolle des Therapeuten

\input{02_TherapeutRolle} \newpage

# Beiträge aus der Community

Nach Veröffentlichung von \projektname{} wird im Git unter
\url{https://gitlab.com/8fragezeichen/} die Möglichkeit bestehen zur
Nutzerdokumentation ebenso wie den restlichen Teilen beizutragen.

Bitte fühlen Sie sich frei, noch nicht geklärte Fragen dort als Issues
zu erstellen.

\vspace{1cm}

*Ihre 8fragezeichen*
