## Atom

### Gibt es einen ähnlich guten Editor wie VSCode?

Etwas länger als das Microsoft-Projekt gibt es bereits [atom](https://atom.io/), dessen Autovervollständigung ist zwar in Spezialfällen etwas langsamer, aber dafür bietet er bereits eine lebhafte Community und viele Erweiterungen.
Weiterhin ist die Dokumentation mit dem [Flight-Manual](http://flight-manual.atom.io/) hervorragend.

#### Installation unter Debian

```sh
#!/bin/sh

# test if installed
dpkg -s atom
e=$?
if [ $e -eq 0 ]; then
	v=$( dpkg -s atom | grep -i version: |cut -d":" -f2- )
	echo -n "atom ist in Version $v	 installiert"
else
	echo "atom ist noch nicht installiert"
fi

#get pakage
url=https://github.com/atom/atom/releases/latest
tmpfile=/tmp/atom-latest-index.html
wget -c $url -O $tmpfile
latest=$( cat $tmpfile | grep -o "href=\".*"|grep '.deb"'|cut -d'"' -f2 )
rm $tmpfile
tmpfile=/tmp/atom-amd64.deb
wget -c $( echo $url|cut -d"/" -f1-3 )$latest -O $tmpfile

# install for debian
sudo dpkg -i $tmpfile
exit

# remove, e.g. before "update" via installing a new version
sudo dpkg -r atom
exit
			
# purge also config, in doubt if you want to uninstall permanently
sudo dpkg -P $( dpkg -S $( which atom ) )
```