## \LaTeX{}

Ein Textsatzsystem, dass sich innerhalb von Markdown nutzen lässt um weitere Möglichkeiten für die Dokumentation nutzbar zu machen.
Für weitere Informationen s. <http://www.dante.de> .

Das MDX-Handbuch, das Prof. Toll erwähnte, ist ebenfalls mit LaTeX gesetzt und
das Werkzeug bietet sich für Texte, die über ein paar Seiten hinausgehen an.

Hier soll Markdown nur ergänzt werden, zuviel soll nicht gemacht werden.

### Manteldokument und Abschnitte

Einzelne Abschnitte können in Dateien ausgelagert werden um eine einfachere Bearbeitung auch mit mehreren Personen zu ermöglichen, hierzu dient das Kommando ``\input``, das Kommando ``\include`` hingegen ermöglicht keine tiefere Verschachtelung und impliziert ggf. Seitenumbrüche.

### Eigene Makros

Mit ``\newcommand`` kann ein neues Makro definiert werden. 

~~~~~
\newcommand{\projektname}{CareGarden}
~~~~~

Beispiel:

* definiert ``\projektname`` und liefert an jedem Vorkommen "CareGarden" zurück

### Label / Referenzen

Verweise und Kreuzverweise innerhalb des Dokumentes sind abbildbar:

* Anker: ``\label{name}`` hinterlegt ein unsichtbares Linkziel "name"
* Link auf Name: ``\nameref{name}`` fügt eine Textreferenz mit Link auf das Ziel "name" an, z.B. "Bildunterschrift"
* Link auf Bezeichner: ``\autoref{name}`` fügt eine Textreferenz mit Link auf das Ziel "name" an, z.B. "Abschnitt X"
* Link auf Seite: ``\pageref{name}`` für eine Seitenreferenz mit Link auf das Ziel "name", z.B. "Seite 25"
* Link auf Nummer: ``\ref{name}`` für ein Referenz mit Link auf das Ziel "name", z.B. "4.4"

Mehr unter: [Wikibooks/LaTeX](https://en.wikibooks.org/wiki/LaTeX/Labels_and_Cross-referencing#autoref)

### Anführungszeichen

Das Paket *csquotes* stellt sprachspezifische Anführungszeichen bereit. Der Aufruf erfolgt mit ``\enquote{text}``.

### Literaturverweise

Ebenfalls möglich sind (externe) Verweise auf üblicherweise Literatur.
Mehr Informationen im [LaTeX-Kompendium](https://de.wikibooks.org/wiki/LaTeX-Kompendium:_Schnellkurs:_Erstellen_eines_Literaturverzeichnisses) oder im [Kursmaterial](https://wwwtcs.inf.tu-dresden.de/~borch/lehre/201516-latex/index.html) des letzten LaTeX-Kurses an der HTW.

* Werk zitieren: ``\cite{name}``
* Seite 1-4 zietieren: ``\cite[S.\,1-4]{name}``
* Nur Werk im Verzeichnis aufführen: ``\nocite{name}``

Ein Minimalbeispiel wurde als doku/demo-Literaturverzeichnis in Form eines aufrufenden Shell-Scripts mit einer TeX-Datei und einer Bib(liografie)-Datei abgelegt. <!-- Quelle: http://latex.hpfsc.de/content/latex_tutorial/literatur_bibtex/ -->

* Für ``\addcontentsline`` s. [Wikibooks/LaTeX](https://de.wikibooks.org/wiki/LaTeX-W%C3%B6rterbuch:_addcontentsline) .
* Für Stiele s. [sharelatex](https://www.sharelatex.com/learn/Biblatex_citation_styles) .
