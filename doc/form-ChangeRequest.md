# Change Request Form -- draft

<!--

example taken from http://www2.cdc.gov/cdcup/library/templates/CDC_UP_Change_Request_Form_Example.doc

parts should be signed with ``echo "Text" | gpg --clearsign -a``

resulting in e.g.

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Text
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.15 (GNU/Linux)

iQIcBAEBCgAGBQJXKgWSAAoJECTLpwatJtn7VZYP/2cZ9ha6CBsrzFuBDOW0ltEk
qEWKRPxf1xI6e8J1cjEjcC3kbgqhCLd+QGhHGMzuAP6KVlXA0ahEJbeQ5K2uR4OW
LQSKXPVyzKXYhuoTIg/MtU+WACqSUAKv80DynHZdaddyxv9qZiPDXoEUe7khhUKd
smL/Nuss0p+PpdyBIkCdBedvGfr4InViKQW1SMpn+/JqW4Mwsp7W6HdB53pvfpvg
3/FUs/wza6hUWc/U+gbfiarM8wcUve6VE5RVNH4tM6dAFiMH5vs+Yk0QqBCmU3b9
DhNJ3KqcvT1p86/SkxeF2Z7Qg34dSfTH+D1avwAyAJb+XviresLpK76eRH486y0h
r16+Imaxh/VUUMncy3aowE1iNYwYZ4hVDex7HiTikwIGPu7p0jcthDTIHcX278r4
J0AxU7lI2yQUvfHnjBBFrJJ3x1FdCBcADCGv9hCym/S8qSC0Wu3L005P310u0sY+
sxLqB+M45CpUHg6p4HB5lmOY52wRzzDyrl06/sD/JKBQJ1058gkbYt8SHWGzB46p
0++/9RKLbRqOrAiB2h252IHHWwKM6J0qFynHYR3027ksxLqy7v5r/RwzM7QP2+nW
DnDKMPjz91aj3ZicwtsM8dXLyBmdiSc8wxVZzns8WK0L+T4vxKAiqwUPnSAfh3L8
/ViW04uEuZBNbI3JZkK/
=TA1+
-----END PGP SIGNATURE-----

-->

{This form is divided into three sections. Section 1 is intended for use by the individual submitting the change request. Section 2 is intended for use by the Project Manager to document/communicate their initial impact analysis of the requested change. Section 3 is intended for use by the Change Control Board (CCB) to document their final decision regarding the requested change.}

## 1. Submitter -- General Information

* Number of CR:	{CR001}
* Type of CR:	{Enhancement / Defect}
* Project/Program/Initiative:	
* Submitter: 	{John Doe}
* Brief Description:	{Stupid typos as for my bad German}
* Date Submitted:	{yymmdd}
* Date Required:	{yymmdd}
* Priority:	{Low / Medium / High / Mandatory}
* Reason for Change:	{why is it requested}
* Affected Artifacts:	{List of affected artefacts}
* Comments:	{references, assumptions and comments regarding the requested change}
* Signing ID:	{PGP Fingerprint}
* Date:	{yymmdd}
* Signature: {PGP Signature of this Block}

## 2. Initial Analysis (Responsibility Manager)

* Time impact: {how long do we need for this}
* Schedule impact: {what is going to be delayed}
* Cost impact: {what will it cost}
* Comments: {comments and recommendation}
* Signing ID:	{PGP Fingerprint}
* Date:	{yymmdd}
* Signature: {PGP Signature of this Block}

## 3. Change Decision (Project Manager)

* Decision:	{Approved / Approved with conditions / Rejected / More Information needed}
* Comments:	{explanation, conditions}
* Signing ID:	{PGP Fingerprint}
* Date:	{yymmdd}
* Signature: {PGP Signature of this Block}
