**Für Definitionen siehe Testglossar** (folgt bald)

### Testplan

Der **Testplan** baut auf folgenden vier Säulen auf:

* Ressourcenplanung
* Teststrategie
* Testpriorisierung
* Werkzeugunterstützung

Hierbei wird bei diesem Projekt das Augenmerk auf **Teststrategie** und **Testpriorisierung** gelegt, da insbesondre die **Teststrategie** die Grundlage für den Testplan sowie für die durchzuführenden Tests bietet. Diese lassen sich wie folgt unterteilen:

1. Komponententest
2. Integrationstest
3. Systemtest
   * Lasttest
4. Abnahmetest

Dabei ist die Reihenfolge der Auflistung einzuhalten. Der Lasttest ist als integraler Bestanddteil des Systemtests zu betrachten, der Abnahmetest ist als eigenständig anzusehen. Für _Care Garden_ wird insbesondere der **Komponententest** und der **Integrationstest** von Bedeutung sein, da Usability in diesem Fall eine höhere Wichtung besitzt als beispielsweise Performanz und die beiden Testverfahren geeignete Methoden bieten, um diese, sowie andere für _Care Garden_ wichtige Faktoren, ausgiebig zu prüfen.

Basierend auf diesen Überlegungen ist folgender Aufbau (als erster Grundriss) entstanden:

### Teststrategie

Für die **Testpriorisierung** soll folgendes einheitliches Priorisierungsshema verwendet werden:

1. höchste Priorität
2. hohe Priorität
3. mittlere Priorität
4. niedrige Priorität

Hierbei bedeutet eine 1. dass der damit gekennzeichneten Bereich ausgiebig und sehr gründlich, dem zufolge über die Mindestanforderung hinaus, beachtet werden muss, eine 4. hingegen kennzeichnet einen Berich als optional beziehungsweise rahmensprengend für das betrachtete Projekt.

Testübergreifend soll folgende **Fehlerklassifikation** verwendet werden:

1. abnahmehinderlicher Fehler 
2. schwerer Fehler
3. mittlerer Fehler
4. leichter Fehler


Aufgrund der gegebenen Anwendungsfälle ist es sinnvoll, die Tests besonders stark an folgende **Rollen** auszurichten:
* Therapeut *T*
* Patient *P*

##### Komponententest

Dieser setzt sich hauptsächlich aus diesen beiden Testverfahren zusammen:

* Black-Box Verfahren
* Glass-Box Verfahren

Es ist anzumerken, dass gerade das **Black-Box** Verfahren auf allen Teststufen, inklusive des **Abnahmetests** angewandt werden sollte.

Für das **Black-Box** Verfahren kann man sich an die **Kontextdiagramme** halten, für das **Glass-Box** Verfahren bietet sich das 
Abarbeiten der **Auslöse-Reaktions-Tabelle** an, wobei hier zuvor eine Priorisierung der einzelnen Kategorien nach obigem Shema erfolgen sollte.

Zur Überprüfung der korrekten Anwendung beider Verfahren lassen sich die **Satzschablonen nach Chris Rupp** zu Hilfe nehmen.

##### Integrationstest

Der **Intergrationstest** lässt sich in folgende Verfahren gliedern:

* Usability-Tests
* Funktionstest
* Szenario-Tests

Alle Tests sollten nach Möglichkeit in der Betriebsumgebng stattfinden.

Für die **Usability-Tests** sollten mindestens zwei von der Entwicklung unabhängige Personen, jeweils in der Rolle des Therapeuten und des Patienten einzeln, aber auch gemeinsam die Anwendung testen. Die hierfür benötigten Testszenarien lassen sich , in Hinblick auf die vorangegangenen Priorisierung, gut aus der **Auslöse-Reaktions-Tabelle** erstellen.

Für den **Funktionstest** und die **Szenario-Tests** lassen sich für die Erstellung der Testszenarien die **Aktivitätsdiagramme** als Grundlage nutzen.

Zur Überprüfung der korrekten Anwendung der Verfahren kann man auch hier die **Satzschablonen nach Chris Rupp** zu Hilfe nehmen.

##### Systemtest

Folgende Verfahren können alle Bestandteil des **Systemtests**:sein:

 * Funktionstest
 * Sicherheitstest
 * Interoperabilitätstest
 * Leistungstest
 * Performanztest
 * Speicherverbrauchtest (oder Ähnliches)
 * Recovery Testing
 * Usability-Test
 * Stresstest
 
Viele dieser Tests sprengen den Umfang des Projektes, **Usability-Tests** und **Funktionstests** werden im **Integratoinstest** weitestgehend abgedeckt. Der **Sicherheitstest** wäre in Hinblick auf den Datenschutz unbedingt ausführlich zu machen, würde aber aufgrund des notwendigen Umfangs den Rahmen des Projektes sprengen.
Dennoch sollten in diesem Fall zumindest rudimentäre Dinge wie Passwortabfragen und Eingaben sowie Sichtbarkeit innerhalb der einzelnen Rollen unbedingt geprüft werden.

Gerade im Punkt **Systemtest** sind die Entwickler der einzelnen Programmteile angehalten, mindestens die Hauptfunktionen ihres Codes zu testen.(Eingaben, überschriebene Variablen, undefiniertes Verhalten..)

### Abnahmetest

Dieser Test bildet den Abschluss und entsteht auf der Grundlage aller zuvor durchgeführten Tests. Für dieses Projekt setzt er sich aus folgenden Testverfahren zusammen:

* Test auf Benutzerakzeptanz (Usability-Test)
* Test auf vertragliche Akzeptanz
* Zielrechnertest
* Robustheitstest
* Dokumentationstest
* Oberflächentests

Es gilt hierbei die oben festgelegte **Fehlerklassifikation**.
Der Abnahmeplan entspricht weitestgehend dem Testplan und wird im Laufe des Projekts entwickelt werden.
