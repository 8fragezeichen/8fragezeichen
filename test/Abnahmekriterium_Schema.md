# Abnahmekriterien



{todo Schema extrahieren und auf ca. 5 AWF beschränken}

### Patientendaten verwalten

#### K1

**Ausgangssituation**

>Therapeut m&ouml;chte Patientendaten anlegen

**Ereignis**

> Zugangsdaten generieren

**Erwartetes Ergebnis**

> Generierte Zugangsdaten anzeigen 


#### K2
**Ausgangssituation**
>Therapeut m&ouml;chte Patientendaten &auml;ndern

**Ereignis**

> Patientendaten &auml;ndern

**Erwartetes Ergebnis**

> Best&auml;igung der &Auml;nderung ? 


#### K3
**Ausgangssituation**
>Therapeut m&ouml;chte Patientendaten l&ouml;schen

**Ereignis**

> Patientendaten werden gel&ouml;scht

**Erwartetes Ergebnis**

> Best&auml;igung des Entfernens 

#### K4
**Ausgangssituation**
>Therapeut m&ouml;chte Patientendaten anzeigen lassen

**Ereignis**

> Patientendaten abrufen

**Erwartetes Ergebnis**

> Patientendaten auf Bildschirm anzeigen 

### Therapeutendaten verwalten

#### K1
**Ausgangssituation**
>Therapeut soll angelegt werden

**Ereignis**

> Entwickler generieren Zugangsdaten

**Erwartetes Ergebnis**

> Generierte Zugangsdaten anzeigen 


#### K2
**Ausgangssituation**
> Therapeutendaten sollen angezeigt werden

**Ereignis**

> Daten werden abgerufen 

**Erwartetes Ergebnis**

> Zugangsdaten anzeigen (Unterschied bei aufrufender Person?) 

> Fehlermeldung im Fehlerfall ?

#### K3
**Ausgangssituation**
>Therapeutendaten sollen ge&auml;ndert werden

**Ereignis**

> Entwickler &auml;ndern Daten

**Erwartetes Ergebnis**

> Best&auml;igung der &Auml;nderung ? 

> Fehlermeldung im Fehlerfall ?

#### K4
**Ausgangssituation**
>Therapeutenpasswort sollen ge&auml;ndert werden

**Ereignis**

> Passwort zur&uuml;setzen 

> Passwortqualit&auml;t pr&uuml;fen ?

**Erwartetes Ergebnis**

> Best&auml;igung der &Auml;nderung 

> Fehlermeldung im Fehlerfall ?

### Zugang verwalten

#### K1
**Ausgangssituation**
>Therapeut/Patient m&ouml;chte sich anmelden

**Ereignis**

> Anmeldeprozess in Gang setzen

**Erwartetes Ergebnis**
> Weiterleitung zur Bedienseite

> Fehlermeldung im Fehlerfall

#### K2
**Ausgangssituation**
>Therapeut/Patient m&ouml;chte sich anmelden

**Ereignis**

> Anmeldeprozess in Gang setzen

**Erwartetes Ergebnis**
> Weiterleitung zur Bedienseite

> Fehlermeldung im Fehlerfall


### Aktivit&auml;tsdaten verwalten

#### K1
**Ausgangssituation**
>Therapeut m&ouml;chte Aktivit&auml;t erstellen

**Ereignis**

> Aktivit&auml;t wird hinzugef&uuml;gt

**Erwartetes Ergebnis**
> Best&auml;tigung der &Auml;nderung

> Anzeigen der &Auml;nderung ?

> Fehlermeldung im Fehlerfall

#### K2
**Ausgangssituation**
>Therapeut m&ouml;chte Aktivit&auml;tsliste einsehen

**Ereignis**

> Aktivit&auml;tsliste wird abgerufen

**Erwartetes Ergebnis**
> Anzeigen der Aktivit&uaml;tsliste auf dem Bildschirm

> Optional: Druckoption anbieten ?

> Fehlermeldung im Fehlerfall

#### K3
**Ausgangssituation**
>Therapeut m&ouml;chte Aktivit&auml;tsdetails

**Ereignis**

> Aktivit&auml;tsliste (Details daraus ?) werden abgerufen

**Erwartetes Ergebnis**
> Anzeigen der Aktivit&uaml;tsdetails auf dem Bildschirm

> Optional: Druckoption anbieten ?

> Fehlermeldung im Fehlerfall

#### K4
**Ausgangssituation**
>Patient m&ouml;chte nach einer Aktivit&auml;t suchen?

**Ereignis**

> Suchanfrage nach Aktivit&auml;t

**Erwartetes Ergebnis**
> Anzeigen der gesuchten Aktivit&uaml;t auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K5
**Ausgangssituation**
>Therapeut m&ouml;chte eine Aktivit&auml;t bearbeiten

**Ereignis**

> Bearbeitungsmaske aufrufen ?

**Erwartetes Ergebnis**
> Weiterleitung zur Bearbeitungsmaske

> Fehlermeldung im Fehlerfall

#### K6
**Ausgangssituation**
>Patient m&ouml;chte eine Aktivit&auml;t als abgeschlossen markieren

**Ereignis**

> Aktivit&auml;t als abgeschlossen markieren

**Erwartetes Ergebnis**
> Best&auml;tigung der &Auml;nderung auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K7
**Ausgangssituation**
>Patient oder Therapeut m&ouml;chte eine Aktivit&auml;t hinzuf&uuml;gen

**Ereignis**

> Aktivit&auml;t hinzuf&uuml;gen

**Erwartetes Ergebnis**
> Best&auml;tigung der &Auml;nderung auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K8
**Ausgangssituation**
>Patient oder Therapeut ? m&ouml;chte eine Aktivit&auml;t l&ouml;schen

**Ereignis**

> Aktivit&auml;t l&ouml;schen 

**Erwartetes Ergebnis**
> Best&auml;tigung des L&ouml;schvorganges auf dem Bildschirm

> Fehlermeldung im Fehlerfall

### Aktivit&auml;splan verwalten
#### K1
**Ausgangssituation**
>Therapeut m&ouml;chte einen Aktivit&auml;tsplan anlegen

**Ereignis**

> Aktivit&auml;tsplan anlegen

**Erwartetes Ergebnis**
> Anzeige des neuen Aktivit&auml;tsplanes auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K2
**Ausgangssituation**
>Therapeut m&ouml;chte einen Aktivit&auml;tsplan bearbeiten

**Ereignis**

> Maske f&uuml;r Bearbeitung aufrufen

**Erwartetes Ergebnis**
> Anzeige der Bearbeitungsmaske

> Fehlermeldung im Fehlerfall

#### K3
**Ausgangssituation**
>Therapeut m&ouml;chte einen Aktivit&auml;tsplan l&ouml;schen

**Ereignis**

>Aktivit&auml;tsplan l&ouml;schen

**Erwartetes Ergebnis**
> Best&auml;tigung des L&ouml;schvorganges auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K4
**Ausgangssituation**
>Therapeut m&ouml;chte einen Aktivit&auml;tsplan l&ouml;schen

**Ereignis**

>Aktivit&auml;tsplan l&ouml;schen

**Erwartetes Ergebnis**
> Best&auml;tigung des L&ouml;schvorganges auf dem Bildschirm

> Fehlermeldung im Fehlerfall

### Nachrichtendaten verwalten
#### K1
**Ausgangssituation**
>Therapeut oder Patient m&ouml;chte eine Nachricht senden

**Ereignis**

> Nachricht senden

**Erwartetes Ergebnis**
> Best&auml;tigung des Sendevorganges auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K2
**Ausgangssituation**
>Therapeut oder Patient m&ouml;chte sich eine Nachricht anzeigen lassen

**Ereignis**

> Nachricht anzeigen

**Erwartetes Ergebnis**
>Anzeigen der Nachricht auf dem Bildschirm

> Fehlermeldung im Fehlerfall

### Datenauswertung verwalten
#### K1
**Ausgangssituation**
>Therapeut oder Patient ? m&ouml;chte sich die Gesamtbilanz anzeigen lassen

**Ereignis**

> Gesamtbilanz abrufen

**Erwartetes Ergebnis**

> Anzeigen der Gesamtbilanz auf dem Bildschirm

> Fehlermeldung im Fehlerfall

#### K2
**Ausgangssituation**
>Therapeut oder Patient ? m&ouml;chte sich die Kennzahlliste anzeigen lassen

**Ereignis**

> Kennzahlliste abrufen

**Erwartetes Ergebnis**

> Anzeigen der Kennzahlliste auf dem Bildschirm

> Fehlermeldung im Fehlerfall
