insert into Aktivität values("A0002","Rückenübung","Liegesütz","Hier kommt eine Beschreibung rein",20,"Zuordnung");
insert into Aktivität values("A0001","Rückenübung","Rumpfbeuge","Hier kommt eine Beschreibung rein",20,"Zuordnung");
insert into Aktivität values("A0003","Stimmübung","Stimmbandtraining","Hier kommt eine Beschreibung rein",20,"Zuordnung");
insert into Aktivität values("A0004","Stimmübung","Gesang","Hier kommt eine Beschreibung rein",20,"Zuordnung");

insert into Patient values("ebrandt","Brandt", "Eric","Ericstraße 4","plz","Dresden","0815/12345","AOK Plus","Hist","ebrandt");
insert into Patient values("fbrandt","Brandt", "Felix","Ericstraße 4","plz","Dresden","0815/12345","AOK Plus","Hist","ebrandt");
insert into Patient values("vkilian","Völkel", "Kilian","Kilianstraße 1","plz","Dresden","0815/12343","AOK Plus","Hist","ebrandt");


insert into Therapeut values("cwolf","Wolf","Cynthia","Logopädie","Praxis für Logopädie","Adresse 5","plz","Dresden","0815/1234","cwolf");
insert into Therapeut values("nsultova","Sultova","Neda","ergotherapie","Praxis für Ergotherapie","Adresse 5","plz","Dresden","0815/1234","nsultova");

insert into PatientTherapeut values("cwolf","ebrandt");