use s72827;

drop table PatientTherapeut;
drop table Aktivitätsplan;
drop table Patient;
drop table Therapeut;
drop table Aktivität;



create table Patient(
	P_ID varchar(20) Primary Key,	
	Name varchar(45),
	Vorname varchar(45),
	Adresse varchar(45),
	Postleitzahl varchar(5),
	Ort varchar(20),
	Telefon varchar(45),
	Diagnose varchar(45),
	Krankenkasse varchar(45),
	Password varchar(45)
);

create table Therapeut(
	T_ID varchar(20) Primary Key,
	Name varchar(45),
	Vorname varchar(45),
	Kategorie varchar(45),
	Praxis varchar(45),
	Adresse varchar(45),
	Postleitzahl varchar(5),
	Ort varchar(20),
	Telefon varchar(15),
	Password varchar(45)
);
	
create Table PatientTherapeut(
	T_ID varchar(20),
	P_ID varchar(20),
	Primary Key(T_ID,P_ID),
	Foreign key (P_ID) references Patient(P_ID) ON DELETE CASCADE,
	Foreign key (T_ID) references Therapeut(T_ID) ON DELETE CASCADE
);

create table Aktivität(
	A_ID varchar(5) Primary Key,
	Name varchar(20),
	Kategorie varchar(45),
	Titel varchar(45),
	Kurzbeschreibung text,
	Detailbeschreibung text
);

/* Image Table für Visualisierung der Aktivitäten - Geplant */

create table Aktivitätsplan(
	T_ID varchar(20),
	P_ID varchar(20),
	A_ID varchar(20),
	start TIMESTAMP,
	ende TIMESTAMP,
	STATUS TIMESTAMP,
	Foreign key (P_ID) references Patient(P_ID) on delete cascade,
	Foreign key (T_ID) references Therapeut(T_ID) on delete cascade,
	Foreign key (A_ID) references Aktivität(A_ID) on delete cascade
);
#################################################
# Muster fuer Trigger in MySQL                  #
# Falls wir das brauchen                        #
#################################################

/*drop trigger new_schema.trig_insert_user
DELIMITER $$

CREATE TRIGGER trig_insert_user
AFTER INSERT ON new_schema.User
FOR EACH row
BEGIN
    DECLARE id varchar(5);

	SET id = NEW.U_ID;

	if id not like "P%%%%%"
	  THEN Insert into Therapeut values(null,NEW.U_ID);
	ELSE Insert into Patient values (null,NEW.U_ID);
	END IF;
END$$
*/

/* FUCKING BULLSHIT TRIGGER RAGE */

DELIMITER ///

CREATE TRIGGER ins_akt BEFORE INSERT ON Aktivität
    FOR EACH ROW
    BEGIN
		
        /* Deklaration der notwendigen Variablen*/
        declare aid varchar(5); 
        declare aid_int int(6);
        declare nullen varchar(4);
        declare var int(6);
        
        /* finden der aktuell hoechsten A_ID*/
        set aid =  (select RIGht((select A_ID from Aktivität
		order by A_ID DESC Limit 1),4)as id);	
		
       
        
        /* Umwandeln in int*/
        set aid_int = cast(aid as UNSIGNED);
        
        set var = (select LENGTH(aid_int+1));
		
        /*Nullen anfuegen*/
		CASE var
            WHEN 1 THEN set nullen = 'A000';
            WHEN 2 THEN set nullen = 'A00';
            WHEN 3 THEN set nullen = 'A0';
            WHEN 4 THEN set nullen = 'A';
   		    else BEGIN
				set aid_int=0;
                set nullen = 'A000';
                END;
        END CASE;
		
        set aid_int = aid_int + 1;
        
        /*Nullen und zahl zusammen */
		set aid = CONCAT(nullen,aid_int);
        
        set NEW.A_ID = aid;
        /*Trage die neuen Daten ein */
        /*INSERT INTO Aktivität (aid,name,Kategorie,Titel,Kurzbeschreibung,Detailbeschreibung) VALUES(NEW.A_ID,NEW.Name,New.Kategorie,NEW.Titel,NEW.Kurzbeschreibung,NEW.Detailbeschreibung);*/
       
    END;
///

DELIMITER ;

