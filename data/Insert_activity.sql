
//Aktivitäten

//Format: ID, Name, Kategorie, Titel, Kurzbeschreibung, Detailbeschreibung

insert into Aktivität values("A0001","Schulterkreisen","Rückenübung","Übung1","Schulterübung um die Muskulatur zu lockern und zu erwärmen.","Locker aufrecht stehen. Ziehen Sie die Schultern hoch und führen Sie sie locker in einer kreisenden Bewegung nach hinten und nach unten. Dauer: etwa 30 Sekunden.");

insert into Aktivität values("A0002","Schulterdrücken","Rückenübung","Übung2","Schulterübung um die Muskulatur zu stärken.","Stellen Sie sich im Abstand von einer Fußlänge an eine Wand und lehnen Sie Ihren Rücken an. Ellenbogen auf Schulterhöhe anheben. Drücken Sie damit Rücken und Schulter von der Wand weg. Fünf Sekunden lang halten, dabei gleichmäßig weiteratmen. Vier- bis fünfmal wiederholen.");

insert into Aktivität values("A0003","Rückentrainer","Rückenübung","Übung3","Rückenübung um den gesamten Rückenbereich durch Anspannung zu stärken.","Knien Sie sich hin und stützen Sie sich mit den Armen ab. Strecken Sie das linke Bein und den rechten Arm bis in die Waagerechte. Der Nacken ist gestreckt und der Rücken gerade, sodass kein Hohlkreuz entsteht. Fünf Sekunden halten und ruhig weiteratmen. Führen Sie dann den rechten Ellenbogen und das linke Knie zusammen. Wiederholen Sie die Übung vier- bis fünfmal und strecken Sie sich anschließend wieder langsam in die Ausgangsposition. Anschließend: Seitenwechsel!");

insert into Aktivität values("A0004","Bauchtrainer","Rückenübung","Übung4","Bauchübung um Bauch- und Rückenmuskulatur zu stärken."," In der Rückenlage die Beine auf einen Hocker legen. Ober- und Unterschenkel sollten annähernd einen rechten Winkel bilden. Kopf und Schultern langsam vom Boden abheben; die Handflächen Richtung Hocker schieben. Position halten. Weiteratmen nicht vergessen! Dann mit Kopf und Schultern in die Ausgangsposition zurück. Die ganze Übung zehnmal ausführen.");


//Quelle:https://www.tk.de/tk/gesunder-ruecken/rueckentraining/rueckenuebungen-zehnminutenprogramm/21298