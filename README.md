# 8fragezeichen: Projekt Software Enegneering II im SS 2016

## Aufgabe

Nach einer eigenen [Idee](./doc/raw/Ausgangssituation.md) finalisiert
[von Prof. Hauptmann](./doc/Aufgabenstellung.md). Wurde in allen Teilen mit 1.0
bewertet; der Projektleiter dankte seinem Team und wurde gefeiert.

## Verantwortungsbereiche

- [Projektleitung](./lead/lead.md)
- Analyse
- [Entwurf](./draft/)
- [Implementation](./impl/)
- [Dokumentation](./doc/)
- Qualitätssicherung
- [Datenbank](./data/)
- [Test](./test/)

## Besprechungen

1. [21.03.2016] 08:00-09:15
2. [04.04.2016] 08:00-09:10
3. [11.04.2016] 08:00-09:15
4. [18.04.2016] 08:00-09:15 mit Prof. Hauptmann
5. [25.04.2016] 08:00-09:10
6. [02.05.2016] 08:00-09:15
7. [09.05.2016] 08:00-09:10
8. [23.05.2016] 08:00-09:15
9. [30.05.2016] 08:00-09:15
10. [06.06.2016] 08:00-09:10
11. [13.06.2016] 08:00-09:00 Frühstück und Foto-Termin


[21.03.2016]: doc/prot/01_160321_Kick-Off-Meeting.pdf
[04.04.2016]: doc/prot/02_160404.pdf
[11.04.2016]: doc/prot/03_160411.pdf
[18.04.2016]: doc/prot/04_160418.pdf
[25.04.2016]: doc/prot/05_160425.pdf
[02.05.2016]: doc/prot/06_160502.pdf
