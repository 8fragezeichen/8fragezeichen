# Projektinformationen

## Aktueller Projektplan

![bild](https://instagantt.com/projects/56e7b730d2822d4a51000097/image?download=now)

## Spontanes Gruppentreffenprotokoll
kleine spontane Treffen unter der Woche. 

| Tag        | Räumlichkeit | Inhalt                   | Mitstreiter      |workload|
|------------|--------------|--------------------------|------------------|--------|
| 24.04.2016  |Z 136b        |AWF, ART                  |Wolf, Richard     |  2,5h      |
| 04.05.2016 | Z 136b       | Packetdiagramm erstellen | Uta, Eric, Felix | 2h     |
|04.05.2016  | Z 136b       | kurzes Review des Codes, Testdaten DB  |Neda,eric,felix,Kilian|1h|
|07.05.2016  | Z 136b       |Verbesserungen Entwurf    | Florian, Felix, Eric|1h   |
|18.05.2016  | Z 136b       | Qualitätsmängel beseitigt  |Uta,Felix,Eric|2h|



#Aktueller Implementations/TestFortschritt 

![bild](https://docs.google.com/spreadsheets/d/17hpVhXAN7TjKhj9VVCr-nEb0LkAMFbbnB_p1N0rjwHg/edit?usp=sharing)