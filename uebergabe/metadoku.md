## Herausforderungen

* off-the-record
  + mündlich
  + bilateral
* Vielzahl an Kommunikationswegen
  + Messenger: Whatsapp, Telegram, Threema, Facebook
  + Portale: Asana, GitLab, InstaGantt, Dropbox
  + Email
* Angst vor Veröffentlichung
  + Unsicherheit bei Urheberrecht
  + Unzufriedenheit mit Qualität
* Überschneidung von Projekten und Personen
  + z.B. Fragen im Kontext von Internet-Technologien
* Öffentlichkeitsarbeit zum Kunden
  + gesonderter Standort für Ergebnisse vorab, s. [www-index.html](http://www2.htw-dresden.de/~s72785/se2/)
